set(LSDALTON_EXTERNAL_LIBS)

if(ENABLE_SCALASCA)
    set(SCALASCA_INSTRUMENT ${CMAKE_Fortran_COMPILER})
    configure_script(
        ${CMAKE_SOURCE_DIR}/src/scalasca.in
        ${PROJECT_BINARY_DIR}/scalascaf90.sh
        )
    set(SCALASCA_INSTRUMENT ${CMAKE_C_COMPILER})
    configure_script(
        ${CMAKE_SOURCE_DIR}/src/scalasca.in
        ${PROJECT_BINARY_DIR}/scalascaCC.sh
        )
    set(SCALASCA_INSTRUMENT ${CMAKE_CXX_COMPILER})
    configure_script(
        ${CMAKE_SOURCE_DIR}/src/scalasca.in
        ${PROJECT_BINARY_DIR}/scalascaCXX.sh
        )
    unset(SCALASCA_INSTRUMENT)
    SET(CMAKE_Fortran_COMPILER "${PROJECT_BINARY_DIR}/scalascaf90.sh")
    SET(CMAKE_C_COMPILER "${PROJECT_BINARY_DIR}/scalascaCC.sh")
    SET(CMAKE_CXX_COMPILER "${PROJECT_BINARY_DIR}/scalascaCXX.sh")
endif()
if(ENABLE_VAMPIRTRACE)
#    set(VAMPIRTRACE_INSTRUMENT ${CMAKE_Fortran_COMPILER})
#    configure_script(
#        ${CMAKE_SOURCE_DIR}/src/vampirtrace.in
#        ${PROJECT_BINARY_DIR}/vampirtracef90.sh
#        )
#    set(VAMPIRTRACE_INSTRUMENT ${CMAKE_C_COMPILER})
#    configure_script(
#        ${CMAKE_SOURCE_DIR}/src/vampirtrace.in
#        ${PROJECT_BINARY_DIR}/vampirtraceCC.sh
#        )
#    set(VAMPIRTRACE_INSTRUMENT ${CMAKE_CXX_COMPILER})
#    configure_script(
#        ${CMAKE_SOURCE_DIR}/src/vampirtrace.in
#        ${PROJECT_BINARY_DIR}/vampirtraceCXX.sh
#        )
#    unset(VAMPIRTRACE_INSTRUMENT)
    SET(CMAKE_Fortran_COMPILER "vtfort")
    SET(CMAKE_C_COMPILER "vtcc")
    SET(CMAKE_CXX_COMPILER "vtc++")
endif()


add_library(
    lsutillib_precision
    ${LSUTIL_PRECISION_SOURCES}
    )

add_library(
    matrixmlib
    ${LSUTIL_MATRIXM_SOURCES}
    )

add_dependencies(matrixmlib lsutillib_precision)

add_library(
    lsutillib_common1
    ${LSUTIL_COMMON_C_SOURCES}
    ${LSUTIL_TYPE_SOURCES}
    )

add_dependencies(lsutillib_common1 matrixmlib)

add_library(
    lsutillib_common2
    ${LSUTIL_COMMON_SOURCES2}
    )

add_dependencies(lsutillib_common2 lsutillib_common1)

add_library(
    lsutillib_common3
    ${LSUTIL_COMMON_SOURCES3}
    )

add_dependencies(lsutillib_common3 lsutillib_common2)

add_library(
    lsutillib_common4
    ${LSUTIL_COMMON_SOURCES4}
    )

add_dependencies(lsutillib_common4 lsutillib_common3)

add_library(
    lsutillib_common5
    ${LSUTIL_COMMON_SOURCES5}
    )

add_dependencies(lsutillib_common5 lsutillib_common4)

add_library(
    lsutillib_common6
    ${LSUTIL_COMMON_SOURCES6}
    )

add_dependencies(lsutillib_common6 lsutillib_common5)

add_library(
    lsutillib_common7
    ${LSUTIL_COMMON_SOURCES7}
    )

add_dependencies(lsutillib_common7 lsutillib_common6)

add_library(
    cuda_gpu_interfaces
    ${CUDA_GPU_INTERFACE_SOURCES}
    )

add_dependencies(cuda_gpu_interfaces lsutillib_common7)

add_library(
    lsutillib_common8
    ${LSUTIL_COMMON_SOURCES8}
    )

add_dependencies(lsutillib_common8 cuda_gpu_interfaces)

add_library(
    matrixolib
    ${LSUTIL_MATRIXO_SOURCES}
    ${LSUTIL_MATRIXO_C_SOURCES}
    )

add_dependencies(matrixolib lsutillib_common8)

add_library(
    matrixulib
    ${LSUTIL_MATRIXU_SOURCES}
    )

add_dependencies(matrixulib matrixolib)

if(ENABLE_RSP)
    set(ExternalProjectCMakeArgs
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
        -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
        -DENABLE_64BIT_INTEGERS=${ENABLE_64BIT_INTEGERS}
        -DPARENT_MODULE_DIR=${PROJECT_BINARY_DIR}/modules
        -DEXTRA_Fortran_FLAGS=${EXTRA_Fortran_FLAGS}
        -DEXTRA_C_FLAGS=${EXTRA_C_FLAGS}
        -DEXTRA_CXX_FLAGS=${EXTRA_CXX_FLAGS}
        )
    add_external(ls-matrix-defop)
    unset(ExternalProjectCMakeArgs)
    set(LSDALTON_EXTERNAL_LIBS
        ${PROJECT_BINARY_DIR}/external/lib/libmatrix-defop.a
        ${PROJECT_BINARY_DIR}/lib/libmatrixulib.a
        ${LSDALTON_EXTERNAL_LIBS}
        )

    add_dependencies(ls-matrix-defop matrixmlib)
    add_dependencies(ls-matrix-defop matrixolib)
    add_dependencies(ls-matrix-defop matrixulib)
endif()

add_library(
    pdpacklib
    ${LSDALTON_FIXED_FORTRAN_SOURCES}
    )
# Bin Gao: matrixulib needs subroutines in pdpacklib
add_dependencies(matrixulib pdpacklib)


add_library(
    lsutiltypelib_common
    ${LSUTIL_TYPEOP_SOURCES}
    )
add_dependencies(lsutiltypelib_common lsutillib_common8)
add_dependencies(lsutiltypelib_common matrixulib)

add_dependencies(lsutiltypelib_common pdpacklib)

add_library(
    lsutillib
    ${LSUTILLIB_SOURCES}
    ${CMAKE_BINARY_DIR}/binary_info.F90
    )

add_dependencies(lsutillib generate_binary_info)

add_dependencies(lsutillib lsutiltypelib_common)

add_library(
    xcfun_interface
    src/xcfun_host/xcfun_host.F90
    )

add_dependencies(xcfun_interface lsutillib_precision)

if(ENABLE_XCFUN)
    set(ExternalProjectCMakeArgs
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
        -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
        -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
        -DEXTRA_Fortran_FLAGS=${EXTRA_Fortran_FLAGS}
        -DEXTRA_C_FLAGS=${EXTRA_C_FLAGS}
        -DEXTRA_CXX_FLAGS=${EXTRA_CXX_FLAGS}
        -DENABLE_64BIT_INTEGERS=${ENABLE_64BIT_INTEGERS}
        )
    add_external(xcfun)
    unset(ExternalProjectCMakeArgs)
    include_directories(${PROJECT_BINARY_DIR}/external/xcfun-build)
    add_dependencies(xcfun_interface xcfun)
    add_definitions(-DVAR_XCFUN)
    set(LSDALTON_EXTERNAL_LIBS
        ${PROJECT_BINARY_DIR}/external/lib/libxcfun_f90_bindings.a
        ${PROJECT_BINARY_DIR}/external/lib/libxcfun.a
        ${LSDALTON_EXTERNAL_LIBS}
        )
endif()


add_library(
    fmmlib
    ${FMM_SOURCES}
    ${FMM_C_SOURCES}
    )


add_dependencies(fmmlib lsutillib_precision)
add_dependencies(fmmlib lsutillib_common1)
add_dependencies(fmmlib lsutillib_common2)
add_dependencies(fmmlib lsutillib_common3)
add_dependencies(fmmlib lsutillib_common4)
add_dependencies(fmmlib lsutillib_common5)
add_dependencies(fmmlib lsutillib_common6)
add_dependencies(fmmlib lsutillib_common7)
add_dependencies(fmmlib lsutillib_common8)
add_dependencies(fmmlib lsutiltypelib_common)

add_library(
    dftfunclib
    ${DFTFUNC_SOURCES}
    ${DFTFUNC_F_SOURCES}
    )

add_dependencies(dftfunclib fmmlib)

add_library(
    lsintlib
    ${LSINT_SOURCES}
    )

add_dependencies(lsintlib dftfunclib)
add_dependencies(lsintlib xcfun_interface)
add_dependencies(lsintlib pdpacklib)
#add_dependencies(lsintlib lsutillib)
add_dependencies(lsintlib lsutillib)
add_dependencies(lsintlib xcfun_interface)

# https://gitlab.com/pett/tensor_lib
include(TensorLibrary)

add_library(
    pbclib
    ${PBC_FORTRAN_SOURCES}
    )

add_dependencies(pbclib lsintlib)

add_library(
    wannierlib
	 ${WANNIER_FORTRAN_SOURCES}
    )

add_dependencies(wannierlib lsintlib)
add_dependencies(wannierlib linearslib)

add_library(
    ddynamlib
    ${DDYNAM_SOURCES}
    )

add_dependencies(ddynamlib lsintlib)

add_library(
    solverutillib
    ${SOLVERUTIL_SOURCES}
    )

add_dependencies(solverutillib lsintlib)

add_library(
    rspsolverlib
    ${RSPSOLVER_SOURCES}
    )

add_dependencies(rspsolverlib solverutillib)

if(ENABLE_RSP)
    set(ExternalProjectCMakeArgs
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
        -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
        -DENABLE_64BIT_INTEGERS=${ENABLE_64BIT_INTEGERS}
        -DPARENT_MODULE_DIR=${PROJECT_BINARY_DIR}/modules
        -DEXTRA_Fortran_FLAGS=${EXTRA_Fortran_FLAGS}
        -DEXTRA_C_FLAGS=${EXTRA_C_FLAGS}
        -DEXTRA_CXX_FLAGS=${EXTRA_CXX_FLAGS}
        )
    add_external(ls-openrsp)
    unset(ExternalProjectCMakeArgs)
    set(LSDALTON_EXTERNAL_LIBS
        ${PROJECT_BINARY_DIR}/external/lib/libopenrsp.a
        ${LSDALTON_EXTERNAL_LIBS}
        )

    add_dependencies(ls-openrsp ls-matrix-defop)
    add_dependencies(ls-openrsp solverutillib)
    add_dependencies(ls-openrsp rspsolverlib)
    add_dependencies(ls-openrsp lsutiltypelib_common)
endif()

add_library(
    linearslib
    ${LINEARS_SOURCES}
    )

add_dependencies(linearslib rspsolverlib)
if(ENABLE_RSP)
add_dependencies(linearslib ls-openrsp)
add_dependencies(linearslib ls-matrix-defop)
endif()


if(ENABLE_DEC)
  add_library(
    declib
    ${DEC_SOURCES}
    )

  add_dependencies(declib lsutiltypelib_common)
  add_dependencies(declib lsutillib_common1)
  add_dependencies(declib lsutillib_common2)
  add_dependencies(declib lsutillib_common3)
  add_dependencies(declib lsutillib_common4)
  add_dependencies(declib lsutillib_common5)
  add_dependencies(declib lsutillib_common6)
  add_dependencies(declib lsutillib_common7)
  add_dependencies(declib lsutillib_common8)
  add_dependencies(declib lsintlib)
  add_dependencies(declib linearslib)
endif()

add_library(
    rsp_propertieslib
    ${RSP_PROPERTIES_SOURCES}
    )
add_dependencies(rsp_propertieslib linearslib)
add_dependencies(rsp_propertieslib lsintlib)
add_dependencies(rsp_propertieslib rspsolverlib)

add_library(
    geooptlib
    ${GEOOPT_SOURCES}
    )

add_dependencies(geooptlib lsintlib)

# QcMatrix
if(ENABLE_QCMATRIX)
    add_subdirectory(src/qcmatrix ${CMAKE_BINARY_DIR}/qcmatrix)
    add_definitions(-DENABLE_LS_QCMATRIX)
    add_dependencies(linearslib ls_qcmatrix_wrapper)
    set(LSDALTON_EXTERNAL_LIBS
        ${LS_QCMATRIX_LIBS}
        ${LSDALTON_EXTERNAL_LIBS})
endif()

# OpenRSP
if(ENABLE_OPENRSP)
    add_subdirectory(src/openrsp ${CMAKE_BINARY_DIR}/openrsp)
    add_definitions(-DENABLE_LS_OPENRSP)
    add_dependencies(linearslib ls_openrsp_wrapper)
    set(LSDALTON_EXTERNAL_LIBS
        ${OPENRSP_LIBS}
        ${LSDALTON_EXTERNAL_LIBS})
    include_directories(${OPENRSP_HEADER_DIR})
    link_directories(${OPENRSP_LIB_DIR})
endif()

add_library(
    lsdaltonmain
    ${LSDALTONMAIN_FORTRAN_SOURCES}
    )

add_dependencies(lsdaltonmain pbclib)
add_dependencies(lsdaltonmain wannierlib)
add_dependencies(lsdaltonmain geooptlib)
add_dependencies(lsdaltonmain linearslib)
if(ENABLE_DEC)
  add_dependencies(lsdaltonmain declib)
endif()
add_dependencies(lsdaltonmain ddynamlib)
add_dependencies(lsdaltonmain rsp_propertieslib)
add_dependencies(lsdaltonmain rspsolverlib)
add_dependencies(lsdaltonmain xcfun_interface)
if(ENABLE_QCMATRIX)
    add_dependencies(lsdaltonmain
                          ls_qcmatrix_wrapper
                          ${LS_QCMATRIX_LIBS})
endif()
if(ENABLE_OPENRSP)
    add_dependencies(lsdaltonmain
                          ls_openrsp_wrapper
                          ${OPENRSP_LIBS})
endif()

if(ENABLE_PCMSOLVER)
    add_dependencies(lsdaltonmain lspcm)
    add_dependencies(lspcm pcmsolver)
    add_dependencies(lsdaltonmain  pcmsolver lspcm)
    add_dependencies(linearslib    pcmsolver lspcm)
    add_dependencies(solverutillib pcmsolver lspcm)
    add_dependencies(lspcm lsutillib lsintlib)
endif()

if(ENABLE_CUDA)
    find_package(CUDA)
endif()
if(CUDA_FOUND)
    # this below is a bit convoluted but here we make
    # sure that the CUDA sources are compiled with GNU always
    # this makes life easier if LSDalton is compiled with Intel
    add_definitions(-DENABLE_CUDA)
    set(ExternalProjectCMakeArgs
        -DCMAKE_C_COMPILER=gcc
        -DCMAKE_CXX_COMPILER=g++
        )
    ExternalProject_Add(cuda_interface
        SOURCE_DIR  ${PROJECT_SOURCE_DIR}/src/cuda
        BINARY_DIR  ${PROJECT_BINARY_DIR}/cuda/build
        STAMP_DIR   ${PROJECT_BINARY_DIR}/cuda/stamp
        TMP_DIR     ${PROJECT_BINARY_DIR}/cuda/tmp
        DOWNLOAD_COMMAND ""
        INSTALL_COMMAND ""
        )
    include_directories(${PROJECT_SOURCE_DIR}/src/cuda)
    set(LSDALTON_EXTERNAL_LIBS
        ${PROJECT_BINARY_DIR}/cuda/build/libcuda_interface.a
        ${CUDA_LIBRARIES}
        ${LSDALTON_EXTERNAL_LIBS}
        )
    add_dependencies(lsdaltonmain cuda_interface)
endif()

if(NOT ENABLE_CHEMSHELL)
    add_executable(
        lsdalton.x
        ${CMAKE_SOURCE_DIR}/src/lsdaltonsrc/lsdalton_wrapper.f90
        ${LINK_FLAGS}
        )

    if(ENABLE_LSLIB)
        add_dependencies(lsdalton.x lslib_tester.x)
    endif()
    

    if(MPI_FOUND)
        # Simen's magic fix for Mac/GNU/OpenMPI
        if(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
            if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
                SET_TARGET_PROPERTIES(lsdalton.x     PROPERTIES LINK_FLAGS "-Wl,-commons,use_dylibs")
            endif()
        endif()
    endif()
endif()


if(ENABLE_LSLIB)
    add_executable(
        lslib_tester.x
        ${LSLIB_SOURCES}
        ${LINK_FLAGS}
        )
    if(MPI_FOUND)
        # Simen's magic fix for Mac/GNU/OpenMPI
        if(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
            if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
                SET_TARGET_PROPERTIES(lslib_tester.x PROPERTIES LINK_FLAGS "-Wl,-commons,use_dylibs")
            endif()
        endif()
    endif()
endif()

set(LSDALTON_LIBS)
set(LSDALTON_LIBS ${LSDALTON_LIBS}
    lsdaltonmain
)
set(LSDALTON_LIBS ${LSDALTON_LIBS}
    xcfun_interface
    geooptlib
    wannierlib
    rsp_propertieslib
    linearslib
    )
if(ENABLE_OPENRSP)
    set(LSDALTON_LIBS ${LSDALTON_LIBS} ls_openrsp_wrapper)
endif()
if(ENABLE_QCMATRIX)
    set(LSDALTON_LIBS ${LSDALTON_LIBS} ls_qcmatrix_wrapper)
endif()
set(LSDALTON_LIBS ${LSDALTON_LIBS}
    rspsolverlib
    solverutillib
    )
if(ENABLE_PCMSOLVER)
	set(LSDALTON_LIBS ${LSDALTON_LIBS} lspcm)
endif()
if(ENABLE_DEC)
  set(LSDALTON_LIBS ${LSDALTON_LIBS} declib)
endif()
set(LSDALTON_LIBS ${LSDALTON_LIBS}
    pbclib
    ddynamlib
    lsintlib
    dftfunclib
    fmmlib
    lsutillib
    lsutiltypelib_common
    pdpacklib
    matrixulib
    matrixolib
    lsutillib_common8
    lsutillib_common7
    lsutillib_common6
    lsutillib_common5
    lsutillib_common4
    lsutillib_common3
    lsutillib_common2
    lsutillib_common1
    matrixmlib
    cuda_gpu_interfaces
    lsutillib_precision
)


#DO NOT ALWAYS USE stdc++ SINCE THIS IS ONLY!!!! THE GNU STDC++ LIB
if(CMAKE_Fortran_COMPILER_ID MATCHES Cray)
   set(USE_GNU_STDCXX_LIB "")
else()
   set(USE_GNU_STDCXX_LIB "stdc++")
endif()

if(NOT ENABLE_CHEMSHELL) #Do not build executable when building chemshell
    #Generates the main LSDALTON executable lsdalton.x
    target_link_libraries(
        lsdalton.x
        ${LSDALTON_LIBS}
        ${EXTERNAL_LIBS}
        ${LSDALTON_EXTERNAL_LIBS}
        ${USE_GNU_STDCXX_LIB}
        ${PCMSOLVER_LIBS}
        )
     add_dependencies(lsdalton.x CheckLSDALTON)
endif()

#Build the LSLIB tester lslib_tester.x
if(ENABLE_LSLIB)
    MERGE_STATIC_LIBS(
        lsdalton
        ${LSDALTON_LIBS}
        )
    target_link_libraries(
        lsdalton
        ${EXTERNAL_LIBS}
        ${PCMSOLVER_LIBS}
        ${LSDALTON_EXTERNAL_LIBS}
        ${USE_GNU_STDCXX_LIB}
        )
    target_link_libraries(
        lslib_tester.x
        lsdalton
        )
    add_dependencies(lsdalton CheckLSDALTON)
endif()

# check the LSDALTON source with a python script
add_custom_command(
   OUTPUT ${PROJECT_BINARY_DIR}/check-source # this is just a dummy
   COMMAND ${CMAKE_COMMAND} -P ${PROJECT_SOURCE_DIR}/cmake/CheckLSDALTON.cmake
   WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
)

add_custom_target(
   CheckLSDALTON
   ALL DEPENDS ${PROJECT_BINARY_DIR}/check-source
)

