# Quick start

Clone the repository:
```
$ git clone --recursive git@gitlab.com:dalton/lsdalton.git
```

Build the code:
```
$ cd lsdalton
$ ./setup [--help]
$ cd build
$ make -j
```

Run the test set:
```
$ ctest [-j4]
```

Dalton Developer’s Guide: http://dalton-devguide.readthedocs.io
