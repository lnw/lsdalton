## Dockerfiles

- `Dockerfile.ubuntu-16.04_gcc-openmpi-mkl`
  1. GCC (C, C++ and Fortran) 5.4.0
  2. CMakei 3.5.1
  3. OpenMPI 1.10.2
  4. Python 2.7.13 from Anaconda
  5. [MKL 2017.0.2 from Anaconda](https://software.intel.com/en-us/articles/using-intel-distribution-for-python-with-anaconda)
- `Dockerfile.ubuntu-16.04_pgi17.4`
  1. PGI 17.4 Community Edition
  2. CMake 3.5.1
  3. Python 2.7.13 from Anaconda (`source activate lsdalton`)
  4. [MKL 2017.0.2 from Anaconda](https://software.intel.com/en-us/articles/using-intel-distribution-for-python-with-anaconda)
  This image **DOES NOT** contain MPI, since I was not able to get OpenMPI to work.

### Building an image

- `docker build -t TAG -f FILENAME .`
