#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00042atoms_0570basfunc_gc_XC.info <<'%EOF%'
   00042atoms_0570basfunc_gc_XC
   -------------
   Molecule:         linear C40H2 molecule/cc-pVDZ
   Wave Function:    B3LYP
   Profile:          Exchange-Correlation Matrix
   CPU Time:         45 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00042atoms_0570basfunc_gc_XC.mol <<'%EOF%'
BASIS
cc-pVDZ
C40H2 molecule based on an optimized C40H2 
structur using b3lyp/6-311+g(d,p)
Atomtypes=2 Nosymmetry Angstrom
Charge=6. Atoms=40
C         0.00000000      0.00000000      1.08290700
C         0.00000000      0.00000000      2.29461600
C         0.00000000      0.00000000      3.64429100
C         0.00000000      0.00000000      4.86890900
C         0.00000000      0.00000000      6.20522000
C         0.00000000      0.00000000      7.43493100
C         0.00000000      0.00000000      8.76635300
C         0.00000000      0.00000000      9.99834400
C         0.00000000      0.00000000     11.32744700
C         0.00000000      0.00000000     12.56059500
C         0.00000000      0.00000000     13.88848500
C         0.00000000      0.00000000     15.12226000
C         0.00000000      0.00000000     16.44947800
C         0.00000000      0.00000000     17.68360100
C         0.00000000      0.00000000     19.01043900
C         0.00000000      0.00000000     20.24475800
C         0.00000000      0.00000000     21.57138600
C         0.00000000      0.00000000     22.80580700
C         0.00000000      0.00000000     24.13232900
C         0.00000000      0.00000000     25.36679500
C         0.00000000      0.00000000     26.69331700
C         0.00000000      0.00000000     27.92778300
C         0.00000000      0.00000000     29.25430500
C         0.00000000      0.00000000     30.48872600
C         0.00000000      0.00000000     31.81535400
C         0.00000000      0.00000000     33.04967300
C         0.00000000      0.00000000     34.37651100
C         0.00000000      0.00000000     35.61063400
C         0.00000000      0.00000000     36.93785200
C         0.00000000      0.00000000     38.17162700
C         0.00000000      0.00000000     39.49951700
C         0.00000000      0.00000000     40.73266500
C         0.00000000      0.00000000     42.06176800
C         0.00000000      0.00000000     43.29375900
C         0.00000000      0.00000000     44.62518100
C         0.00000000      0.00000000     45.85489200
C         0.00000000      0.00000000     47.19120300
C         0.00000000      0.00000000     48.41582100
C         0.00000000      0.00000000     49.76549600
C         0.00000000      0.00000000     50.97720500
Charge=1. Atoms=2
H         0.00000000      0.00000000      0.00000000
H         0.00000000      0.00000000     52.06011200
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00042atoms_0570basfunc_gc_XC.dal <<'%EOF%'
**PROFILE
.XC
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.START
H1DIAG
.GCBASIS
*DFT INPUT
.GRID4
.GRID TYPE
BLOCK
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00042atoms_0570basfunc_gc_XC.check
cat >> 00042atoms_0570basfunc_gc_XC.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange Correlation energy = * \-208\.3645363" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=2
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
