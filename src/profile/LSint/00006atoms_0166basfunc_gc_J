#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00006atoms_0166basfunc_gc_J.info <<'%EOF%'
   00006atoms_0166basfunc_gc_J
   -------------
   Molecule:         C2H4 molecule/(mixed cc-pVTZ/cc-pVQZ)
   Wave Function:    HF
   Profile:          Coulomb Matrix
   CPU Time:         ~17 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00006atoms_0166basfunc_gc_J.mol <<'%EOF%'
ATOMBASIS
C2H4 molecule
------------------------------
AtomTypes=2 Nosymmetry
Charge=1.  Atoms=4  Bas=cc-pVTZ
H                 10.474038    0.000000  -11.443585
H                  8.625986   -0.000022  -11.443616
H                  8.625962   -0.000019   -8.930499
H                 10.474014    0.000026   -8.930468
Charge=6.  Atoms=2  Bas=cc-pVQZ
C                  9.550000    0.000000  -10.850000
C                  9.550000    0.000000   -9.524084
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00006atoms_0166basfunc_gc_J.dal <<'%EOF%'
**PROFILE
.COULOMB
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DIAG
.GCBASIS
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00006atoms_0166basfunc_gc_J.check
cat >> 00006atoms_0166basfunc_gc_J.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Coulomb energy, mat_dotproduct\(D,J\)\= * 140\.85381637" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
