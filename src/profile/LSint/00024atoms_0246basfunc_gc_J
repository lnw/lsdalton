#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00024atoms_0246basfunc_gc_J.info <<'%EOF%'
   00024atoms_0246basfunc_gc_J
   -------------
   Molecule:         phenanthrene/cc-pVDZ
   Wave Function:    HF
   Profile:          Coulomb Matrix
   CPU Time:         ~1 min
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00024atoms_0246basfunc_gc_J.mol <<'%EOF%'
BASIS
cc-pVDZ
phenanthrene optimized with b3lyp/6-31G(d,p)
-------------------
Atomtypes=2 Nosymmetry Angstrom
Charge=6.0 Atoms=14
C1        0.000000      0.713073      3.659934
C2        0.000000      1.407007      2.479237
C3        0.000000      0.722620      1.223550
C4        0.000000     -0.722620      1.223550
C5        0.000000     -1.407007      2.479237
C6        0.000000     -0.713073      3.659934
C7        0.000000      1.403564      0.000122
C8        0.000000     -1.403564      0.000122
C9        0.000000     -0.722622     -1.223850
C10       0.000000      0.722622     -1.223850
C11       0.000000      1.407050     -2.479073
C12       0.000000     -1.407050     -2.479073
C13       0.000000     -0.712866     -3.659895
C14       0.000000      0.712866     -3.659895
Charge=1.0 Atoms=10
C1        0.000000      1.246358      4.606007
C2        0.000000     -1.246358      4.606007
C3        0.000000      2.493904      2.476932
C4        0.000000     -2.493904      2.476932
C5        0.000000      2.491301     -0.000287
C6        0.000000     -2.491301     -0.000287
C7        0.000000      2.493934     -2.476666
C8        0.000000     -2.493934     -2.476666
C9        0.000000     -1.245736     -4.606137
C10       0.000000      1.245736     -4.606137
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00024atoms_0246basfunc_gc_J.dal <<'%EOF%'
**PROFILE
.COULOMB
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DIAG
.GCBASIS
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00024atoms_0246basfunc_gc_J.check
cat >> 00024atoms_0246basfunc_gc_J.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Coulomb energy, mat_dotproduct\(D,J\)\= * 1366\.966695" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
