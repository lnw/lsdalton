! This file is copied from OpenRSP repository, for temporary use; it will be
! replaced by another libary soon

module interface_nuclear

   use TYPEDEFTYPE, only: LSSETTING

   implicit none

   public nuclear_potential
   public aatnuc_ifc
   public get_nuc_dipole_gradient

contains

  !> (derivatives of) nuclear repulsion and nuclei--field interaction
  subroutine nuclear_potential(deriv, ncor, ls_setting, field, ncomp, nucpot)
    !> order of geometry derivative requested
    integer,      intent(in)  :: deriv
    !> number of geometry coordinates
    integer,      intent(in)  :: ncor
    type(LSSETTING), intent(in) :: ls_setting
    !> external potential labels, or 'NONE'
    character(4), intent(in)  :: field(2)
    !> number of components of the field (must be all, ie. 1/3/6)
    integer,      intent(in)  :: ncomp
    !> output tensor
    real(8),      intent(out) :: nucpot(ncor**deriv * ncomp)
    !-------------------------------------------------------
    integer i, j, na
    ! early return if deriv higher than moment (zero)
    if ((field(1) == 'EL  ' .and. deriv > 1) .or. &
        (field(1) == 'ELGR' .and. deriv > 2)) then
        nucpot(:) = 0
        return
    end if
    na = ncor/3
    ! ---- switch by field(1), field(2) and deriv
    ! all orders of nuclear repulsion
    if (field(1) == 'NONE') then
       call nucrep_deriv(na, ls_setting, deriv, nucpot)
    ! minus dipole moment gradient
    else if (field(1) == 'EL  ' .and. deriv == 1) then
       call get_nuc_dipole_gradient(na, ls_setting, nucpot)
       nucpot = -nucpot !d/dR dip = -d2E/dR/dF
    ! atomic axial tensor
    else if (field(1) == 'MAG ' .and. field(2) == 'NONE' .and. deriv == 1) then
       call aatnuc_ifc(na, ls_setting, nucpot)
       ! change sign, transpose
       nucpot(:) = (/-nucpot(1::3), -nucpot(2::3), -nucpot(3::3)/)
    else
       print *,'rsp_backend nuclear_potential error: not implented, deriv =', &
               deriv, '  field = ', field
       call lsquit('rsp_backend nuclear_potential error: not implented', 6)
    end if
  end subroutine

  ! derivatives of nuclear repulsion energy
  subroutine nucrep_deriv(na, ls_setting, deriv, nucrep)

    integer, intent(in)  :: na, deriv
    type(LSSETTING), intent(in) :: ls_setting
    real(8), intent(out) :: nucrep((3*na)**deriv)
    !--------------------------------------------
    real(8) displ(3), power(3**deriv)
    integer nuci, nucj, k, l, m, ncor_pow(deriv)
    ! powers of ncor=3*na, for navigating through nucrep
    ncor_pow(:) = (/((3*na)**k, k=0,deriv-1)/)
    ! start from zero
    nucrep(:) = 0
    ! loop over pairs i<j of nuclei
    do nucj = 2, na
       do nuci = 1, nucj-1
          ! internuclear displacement
          displ(:) = ls_setting%MOLECULE(1)%p%ATOM(nucj)%CENTER(:) &
                   - ls_setting%MOLECULE(1)%p%ATOM(nuci)%CENTER(:)
          ! first unperturbed repulsion energy
          power(1) = ls_setting%MOLECULE(1)%p%ATOM(nuci)%Charge &
                   * ls_setting%MOLECULE(1)%p%ATOM(nucj)%Charge / sqrt(sum(displ**2))
          ! construct deriv'th tensor power of displacement vector,
          ! carrying the prefactor QiQj (2k-1)!! / r^(2k+1)
          do k = 1, deriv
             l = 3**(k-1)
             power(2*l+1:3*l) = power(:l) * displ(3) * (2*k-1) / sum(displ**2)
             power(  l+1:2*l) = power(:l) * displ(2) * (2*k-1) / sum(displ**2)
             power(     :  l) = power(:l) * displ(1) * (2*k-1) / sum(displ**2)
          end do
          ! remove traces from displacement power
          if (deriv >= 2) call remove_traces(deriv, power)
          ! add traceless displacement power at the 2^deriv different
          ! places in the nucrep derivative tensor
          call place_power_tensor
       end do
    end do

  contains

    recursive subroutine remove_traces(m,a)
      integer, intent(in)    :: m
      real(8), intent(inout) :: a(3**(m-2),3,3)
      real(8) b(3**(m-2))
      integer i, j
      b(:) = a(:,1,1) + a(:,2,2) + a(:,3,3)
      if (m >= 4) call remove_traces(m-2,b)
      do j = 2, m
         do i = 1, j-1
            call subtract_from_diag( &
                         (deriv+m-1) * (1+(deriv-m)/2), &
                         3**(i-1), 3**(j-i-1), 3**(m-j), b, a)
         end do
      end do
    end subroutine

    subroutine subtract_from_diag(denom, l, m, t, b, a)
      integer, intent(in)    :: denom, l, m, t
      real(8), intent(in)    :: b(l,m,t)
      real(8), intent(inout) :: a(l,3,m,3,t)
      a(:,1,:,1,:) = a(:,1,:,1,:) - b(:,:,:)/denom
      a(:,2,:,2,:) = a(:,2,:,2,:) - b(:,:,:)/denom
      a(:,3,:,3,:) = a(:,3,:,3,:) - b(:,:,:)/denom
    end subroutine

    subroutine place_power_tensor
      integer offset, i, j, y, z, k, s
      logical neg
      offset = (3*(nuci-1)) * sum(ncor_pow)
      neg = .false.
      ! loop over all index permutations iiiii jiiii ... ijjjj jjjjj
      do k = 0, 2**deriv - 1
         ! loop over Cartesian indices xxxxx yxxxx ... yzzzz zzzzz
         i = offset + 1
         y = 0
         z = 0
         do j = 1, 3**deriv
            ! add or set
            if (k==0 .or. k == 2**deriv-1) then
               nucrep(i) = nucrep(i) + merge(-1,1,neg) * power(j)
            else
               nucrep(i) = merge(-1,1,neg) * power(j)
            end if
            ! increment index i
            do s = 0, deriv-1
               ! x -> y transition
               if (.not.btest(y,s) .and. .not.btest(z,s)) then
                  y = ibset(y,s)
                  i = i + ncor_pow(s+1)
                  exit
               ! y -> z transition
               else if (btest(y,s)) then
                  y = ibclr(y,s)
                  z = ibset(z,s)
                  i = i + ncor_pow(s+1)
                  exit
               ! z -> x transition (followed by 'carry')
               else
                  z = ibclr(z,s)
                  i = i - 2*ncor_pow(s+1)
               end if
            end do
         end do
         ! increment offset ofs for next index permutation
         do s = 0, deriv-1
            neg = .not.neg
            if (.not.btest(k,s)) then
               offset = offset + 3*(nucj-nuci) * ncor_pow(s+1)
               exit
            else
               offset = offset - 3*(nucj-nuci) * ncor_pow(s+1)
            end if
         end do
      end do
    end subroutine

  end subroutine

   subroutine get_nuc_dipole_gradient(nr_atoms, ls_setting, dipole_gradient)

      integer, intent(in)  :: nr_atoms
      type(LSSETTING), intent(in) :: ls_setting
      real(8), intent(out) :: dipole_gradient(3*nr_atoms, 3)

      integer              :: iatom
      integer              :: ixyz
      integer              :: n

      dipole_gradient = 0.0d0
      n = 0
      do iatom = 1, nr_atoms
         do ixyz = 1, 3
            n = n + 1
            dipole_gradient(n, ixyz) = ls_setting%MOLECULE(1)%p%ATOM(iatom)%Charge
         end do
      end do

   end subroutine


   !> Nuclear contribution to the atomic axial tenaor (AAT),
   !> needed for vibrational circular dichroism (VCD)
   !> In the quasienergy formalism, the AAT is:
   !> d^3E/dR(-w)dB(w)dw |w=0
   subroutine aatnuc_ifc(na, ls_setting, tensor)

      integer, intent(in)  :: na
      type(LSSETTING), intent(in) :: ls_setting
      real(8), intent(out) :: tensor(3, 3*na)

      integer :: k, l, ia, ib
      integer :: lc(3, 3)
      real(8) :: flc(3, 3)

      lc = 0
      lc(1, 2) = 3
      lc(2, 1) = 3
      lc(1, 3) = 2
      lc(3, 1) = 2
      lc(2, 3) = 1
      lc(3, 2) = 1

      flc = 0.0d0
      flc(1, 2) =  1.0d0
      flc(2, 1) = -1.0d0
      flc(1, 3) = -1.0d0
      flc(3, 1) =  1.0d0
      flc(2, 3) =  1.0d0
      flc(3, 2) = -1.0d0

      tensor = 0.0d0
      l = 0
      do k = 1, na
         do ia = 1, 3
            l = l + 1
            do ib = 1, 3
               tensor(ib, l) = ls_setting%MOLECULE(1)%p%ATOM(k)%Charge             &
                             * ls_setting%MOLECULE(1)%p%ATOM(k)%CENTER(lc(ia, ib)) &
                             * flc(ia, ib)
            end do
         end do
      end do
      tensor = 0.25d0*tensor

   end subroutine

end module
