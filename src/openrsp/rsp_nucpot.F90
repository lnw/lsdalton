! This file is modified from OpenRSP repository, for temporary use; it will be
! replaced by another libary soon

  ! MaR: Seems to work properly, but memory usage is not tensor symmetry nonredundant
  !> Contribution from nuclear repulsion and nuclei--field interaction
  !> to response functions. Fields (type rsp_field) are here in arbitrary order.
  !> (in normal mode) Fields are sorted, component ranges extended to what
  !> rsp_backend expects (currently only full ranges). Then the call is then relayed
  !> to rsp_backend's nuclear_potential, which computes and returns the requested real
  !> tensor in standard order. The requested ranges of this tensor is then reordered
  !> and added to rspfunc
  subroutine rsp_nucpot(nuc_num_pert,   &
                        nuc_order_geo,  &
                        num_atoms,      &
                        ls_setting,     &
                        ext_label,      &
                        ext_ncomp,      &
                        pert_idx_order, &
                        propsize,       &
                        rspfunc_output)
    use precision, only: INTK, realk
    use TYPEDEFTYPE, only: LSSETTING
    use interface_nuclear, only: nuclear_potential
    implicit none
    integer(kind=INTK), intent(in) :: nuc_num_pert
    integer(kind=INTK), intent(in) :: nuc_order_geo
    integer(kind=INTK), intent(in) :: num_atoms
    type(LSSETTING), intent(in) :: ls_setting
    !> external potential labels, or 'NONE'
    character(4), intent(in) :: ext_label(2)
    !> number of components of the field (must be all, ie. 1/3/6)
    integer(kind=INTK), intent(in) :: ext_ncomp
    integer(kind=INTK), intent(in) :: pert_idx_order(nuc_num_pert)
    integer(kind=INTK), intent(in) :: propsize
    real(kind=realk), intent(inout) :: rspfunc_output(propsize)
    integer(kind=INTK) num_coord
    integer(kind=INTK) tcomp(nuc_num_pert)
    integer(kind=INTK) i
    !> tmp tensor, to which nuclear contribution is *ADDED*
    real(kind=realk), allocatable :: rspfunc(:)
    ! use inner to avoid allocate'ing 'nucpot' below
    num_coord = 1
    if (nuc_order_geo>0) num_coord = num_atoms*3
    do i = 1, nuc_order_geo
      tcomp(i) = num_coord
    end do
    if (nuc_order_geo==nuc_num_pert-1) then
      if (ext_label(1)=='EL  ' .or. &
          ext_label(1)=='MAG0' .or. &
          ext_label(1)=='MAG ') then
        tcomp(nuc_num_pert) = 3
      else if (ext_label(1)=='ELGR') then
        tcomp(nuc_num_pert) = 6
      else
        call lsquit('rsp_nucpot>> invalid field label', 6)
      end if
    else if (nuc_order_geo==nuc_num_pert-2) then
      if (ext_label(1)=='EL  ' .or. &
          ext_label(1)=='MAG0' .or. &
          ext_label(1)=='MAG ') then
        tcomp(nuc_num_pert-1) = 3
      else if (ext_label(1)=='ELGR') then
        tcomp(nuc_num_pert-1) = 6
      else
        call lsquit('rsp_nucpot>> invalid field label', 6)
      end if
      if (ext_label(2)=='EL  ' .or. &
          ext_label(2)=='MAG0' .or. &
          ext_label(2)=='MAG ') then
        tcomp(nuc_num_pert) = 3
      else if (ext_label(2)=='ELGR') then
        tcomp(nuc_num_pert) = 6
      else
        call lsquit('rsp_nucpot>> invalid field label', 6)
      end if
    end if
    allocate(rspfunc(product(tcomp)), stat=i)
    if (i/=0) then
      call lsquit('rsp_nucpot>> allocate rspfunc', 6)
    end if
    rspfunc = 0.0
    call inner
    deallocate(rspfunc)
  contains
    subroutine inner
      real(kind=realk) nucpot(num_coord**nuc_order_geo * ext_ncomp)
      integer h, i, j, k, m, n, p
      call nuclear_potential(nuc_order_geo, &
                             num_coord,     &
                             ls_setting,    &
                             ext_label,     &
                             ext_ncomp,     &
                             nucpot)
      ! add requested component ranges to rspfunc
      call permute_selcomp_add(1d0,                      &
                               nuc_num_pert,             &
                               pert_idx_order,           &
                               (/(1,i=1,nuc_num_pert)/), &
                               tcomp,                    &
                               tcomp,                    & !FIXME: is it right?
                               nucpot,                   &
                               rspfunc)

! MaR: SIMPLE LOOPS TO ASSIGN VALUES WITH ONLY GEOMETRIC PERTURBATIONS

      if (nuc_order_geo == nuc_num_pert) then

         if (nuc_order_geo == 1) then

            rspfunc_output = rspfunc

         else if (nuc_order_geo == 2) then

            h = 0
            do i = 1, num_coord
               do j = i, num_coord
                  h = h + 1
                  rspfunc_output(h) = rspfunc((i - 1)*num_coord + j)
               end do
            end do

         else if (nuc_order_geo == 3) then

            h = 0
            do i = 1, num_coord
               do j = i, num_coord
                  do k = j, num_coord
                     h = h + 1
                     rspfunc_output(h) = rspfunc((i - 1)*num_coord**2 + (j - 1)*num_coord + k)
                  end do
               end do
            end do

         else if (nuc_order_geo == 4) then

            h = 0
            do i = 1, num_coord
               do j = i, num_coord
                  do k = j, num_coord
                     do m = k, num_coord
                        h = h + 1
                        rspfunc_output(h) = rspfunc((i - 1)*num_coord**3 + (j - 1)*num_coord**2 + &
                                            (k - 1)*num_coord + m)
                     end do
                  end do
               end do
            end do

         else if (nuc_order_geo == 5) then

            h = 0
            do i = 1, num_coord
               do j = i, num_coord
                  do k = j, num_coord
                     do m = k, num_coord
                        do n = m, num_coord
                           h = h + 1
                           rspfunc_output(h) = rspfunc((i - 1)*num_coord**4 + (j - 1)*num_coord**3 + &
                                                       (k - 1)*num_coord**2 + (m - 1)*num_coord + n)
                        end do
                     end do
                  end do
               end do
            end do

         else if (nuc_order_geo == 6) then

            h = 0
            do i = 1, num_coord
               do j = i, num_coord
                  do k = j, num_coord
                     do m = k, num_coord
                        do n = m, num_coord
                           do p = n, num_coord
                              h = h + 1
                              rspfunc_output(h) = rspfunc((i - 1)*num_coord**5 + &
                              (j - 1)*num_coord**4 + (k - 1)*num_coord**3 + &
                              (m - 1)*num_coord**2 + (n - 1)*num_coord + p)
                           end do
                        end do
                     end do
                  end do
               end do
            end do


         else if (nuc_order_geo > 6) then

            !write(*,*) 'rsp_nucpot error: No support for nuc_order_geo > 6 yet'
            call lsquit('rsp_nucpot error: No support for nuc_order_geo > 6 yet', 6)

         else

            !write(*,*) 'rsp_nucpot error: Unknown field setup'
            call lsquit('rsp_nucpot error: Unknown field setup', 6)

         end if

         else if (nuc_order_geo == (nuc_num_pert - 1)) then

            if (nuc_num_pert == 2) then

               write(*,*) 'rsp_nucpot warning: Generally untested support for one non-geo. field with nuc_num_pert = 2'

               h = 0
               do i = 1, num_coord
                  do j = 1, 3
                     h = h + 1
                     rspfunc_output(h) = rspfunc(num_coord * (j - 1) + i)
                  end do
               end do

            else if (nuc_num_pert == 1) then

               write(*,*) 'rsp_nucpot warning: Generally untested support for one non-geo. field with nuc_num_pert = 1'

               rspfunc_output = rspfunc
                              
            else

               rspfunc_output = 0.0

            end if

         else

            rspfunc_output = 0.0

         end if

       end subroutine
  end subroutine

  !> Add the average contribution 'ave', with dimensions 'dima' to response tensor
  !> 'rsp', with dimensions 'dimr', but where dimensions are permuted by 'perm'.
  !> 'idxr' selects (if >0) which starting components in 'ave' for each dim in 'rsp',
  !> or (if <0) index of a density matrix dimension.
  !> @prefac  sign or prefactor: rsp = rsp + prefac * ave
  !> @ndim    number of dimensions
  !> @perm    ordering of dimensions. For each dim in ave, the corresponding dim in rsp
  !> @idxr    starting component in ave for each dim in rsp, or if negative, the index
  !>          of a density dimension in rsp
  !> @dima    dimensions of ave. Density dimensions must have dim 1
  !> @dimr    dimensions of rsp
  !> @ave     real array of integral averages, as from integral program
  !> @rsp     complex respons function tensor
  subroutine permute_selcomp_add(prefac, ndim, perm, idxr, dima, dimr, ave, rsp)
    use precision, only: INTK, realk
    implicit none
    real(kind=realk), intent(in) :: prefac
    integer, intent(in) :: ndim, perm(ndim), idxr(ndim)
    integer, intent(in) :: dima(ndim), dimr(ndim)
    real(kind=realk), intent(in) :: ave(product(dima))
    real(kind=realk), intent(inout) :: rsp(product(dimr))
    integer i, ia, ir, stpr(ndim), stpa(ndim), ii(ndim), dd(ndim)
    ! calculate dimension steps in ave (cumulative products of
    ! dimensions), as well as offset due to starting indices idxr (ia)
    ia = 0
    do i = 1, ndim
       if (i==1) stpa(i) = 1
       if (i/=1) stpa(i) = stpa(i-1) * dima(i-1)
       if (idxr(perm(i)) > 0) & !positive means starting comp
          ia = ia + stpa(i) * (idxr(perm(i)) - 1)
    end do
    ! calculate (permuted) dimension steps in rsp, and offset due to
    ! density indices (ir), and permuted dimensions (dd)
    ir = 0
    do i = 1, ndim
       if (i==1) stpr(perm(i)) = 1
       if (i/=1) stpr(perm(i)) = stpr(perm(i-1)) * dimr(i-1)
       if (idxr(i) <= 0) then !negative means density index
          ir = ir + stpr(perm(i)) * (-idxr(i) - 1)
          dd(perm(i)) = 1
       else
          dd(perm(i)) = dimr(i)
       end if
    end do
    ! loop over indices in ave and rsp
    ii = 0 !indices from zero to dd-1
    do
       rsp(ir+1) = rsp(ir+1) + prefac * ave(ia+1)
       ! increment indices
       do i = 1, ndim
          ii(i) = ii(i) + 1
          ia = ia + stpa(i)
          ir = ir + stpr(i)
          if (ii(i) /= dd(i)) exit
          ii(i) = 0
          ia = ia - stpa(i) * dd(i)
          ir = ir - stpr(i) * dd(i)
       end do
       if (i == ndim+1) exit
    end do
  end subroutine
