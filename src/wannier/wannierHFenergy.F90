module wannier_HF

  use precision
  use wannier_types
  use lattice_storage
  use wlattice_domains
  use matrix_module, only: &
       matrix
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_tr, &
       mat_add, &
       mat_trAB
!-tbp: stuff for testing
  use wannier_fockMO, only: &
       w_calc_orthpot_mo
  use wannier_asymptotic_expansion
  use molecule_typetype, only: &
       moleculeinfo
  
  implicit none
  private
  public :: wannier_HF_energy, wannier_P_energy

contains

  function wannier_HF_energy(dens, fockmat, smat, tmat, zmat, enn, oneptoper, &
       wconf, refcell, lupri, luerr) result(EHF)
    
    implicit none
    type(lmatrixp), intent(inout)       :: dens, fockmat, oneptoper, tmat, zmat, smat
    real(realk), intent(in)             :: enn
    type(wannier_config), intent(inout) :: wconf
    type(moleculeinfo), intent(in)      :: refcell
    real(realk)                         :: EHF, EHFAB, Ekin, EZ
    integer, intent(in), optional       :: lupri, luerr

    type(lattice_domains_type) :: dom
    integer                    :: m, indx_m, nbast, i
    type(matrix)               :: F_0M, H_0M, D_M0, tmp, tmp2, T_0M, Z_0M, FF_0M
    logical                    :: lstat_0M, lstat_M0
    real(realk)                :: trace
    type(lmatrixp)             :: ZAEmat
    type(matrix),pointer       :: Z_pt

    !Initialization of the domain on 2d0
    call ltdom_init(dom, wconf%blat%dims, maxval(dens%cutoffs))
    call ltdom_getdomain(dom, dens%cutoffs, incl_redundant=.true.)

    !Allocation of the lattice matrices
    nbast=dens%nrow

    if (wconf%asymptotic_expansion) then
       call lts_init(ZAEmat, wconf%blat%dims, fockmat%cutoffs, nbast, nbast, &
            & wconf%no_redundant, .false.)
       loopm: do m=1, dom%nelms
          indx_m = dom%indcs(m)
          call lts_mat_init(ZAEmat,indx_m)
          Z_pt => lts_get(ZAEmat,indx_m)
          call mat_zero(Z_pt)
       enddo loopm
       call wcalc_ZAE(ZAEmat,smat,refcell,wconf,lupri,luerr)
    endif
       
    
    call mat_init(D_M0,nbast,nbast)
    call mat_init(F_0M,nbast,nbast)
    call mat_init(H_0M,nbast,nbast)
    call mat_init(T_0M,nbast,nbast)
    call mat_init(Z_0M,nbast,nbast)
    call mat_init(FF_0M,nbast,nbast)
    call mat_init(tmp,nbast,nbast)
    call mat_zero(tmp)
    call mat_init(tmp2,nbast,nbast)
    call mat_zero(tmp2)

    EHFAB = 0.0_realk
    Ekin = 0.0_realk
    EZ = 0.0_realk
    loop_m: do m=1, dom%nelms
       indx_m = dom%indcs(m)
       
       call mat_zero(F_0M)
       call lts_copy(fockmat,indx_m,lstat_0M,F_0M)

       call mat_zero(H_0M)
       call lts_copy(oneptoper,indx_m,lstat_0M,H_0M)

       call mat_zero(D_M0)
       call lts_copy(dens,-indx_m,lstat_M0,D_M0)

       call mat_zero(T_0M)
       call lts_copy(tmat,indx_m,lstat_0M,T_0M)

       call mat_zero(Z_0M)
       call lts_copy(zmat,indx_m,lstat_0M,Z_0M)

       if (wconf%asymptotic_expansion) then
          call mat_zero(FF_0M)
          call lts_copy(ZAEmat,indx_m,lstat_0M,FF_0M)
       endif
          
       if (.not.(lstat_0M .and. lstat_M0)) cycle
       call mat_add(1.0_realk,F_0M,-1.0_realk,H_0M,tmp)

       EKin = EKin + mat_trab(T_0M,D_M0)
       
       if (wconf%asymptotic_expansion) then 
          call mat_add(1.0_realk,tmp,-1.0_realk,FF_0M,tmp2)
          EHFAB = EHFAB + mat_trab(tmp2,D_M0)
          EZ = EZ + mat_trab(Z_0M,D_M0) + mat_trab(FF_0M, D_M0)
       else
          EHFAB = EHFAB + mat_trab(tmp,D_M0)
          EZ = EZ + mat_trab(Z_0M,D_M0) 
       endif
    enddo loop_m
    
    !EHF = mat_tr(tmp2) + enn
    EHF = EHFAB + enn + 2*Ekin + 2*EZ

    if (present(lupri)) then
       write(lupri,*) ''
       write(lupri,*) 'Kinetic Energy                ', 2*Ekin
       write(lupri,*) 'Electron-nucleus Interaction  ', 2*EZ
       write(lupri,*) 'Electron-electron Interaction ', EHFAB
       write(lupri,*) 'Nucleus-nucleus Interaction   ', enn
       write(lupri,*) ''
       !write(lupri,*) 'HF energy                     ', EHF
       !write(lupri,*) ''
    endif
    
    !Free the lattice matrices
    call mat_free(F_0M)
    call mat_free(H_0M)
    call mat_free(D_M0)
    call mat_free(T_0M)
    call mat_free(Z_0M)
    call mat_free(FF_0M)
    call mat_free(tmp)
    call mat_free(tmp2)
        
    !Free the domain
    call ltdom_free(dom)

    if (wconf%asymptotic_expansion) then 
       call lts_free(ZAEmat)
    endif

  end function wannier_HF_energy

  function wannier_P_energy(orthpot, cmo, wconf, lupri, luerr) result(trP)
    type(llmatrixp), intent(inout) :: orthpot
    type(lmatrixp), intent(inout) :: cmo
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in), optional :: lupri, luerr
    real(realk) :: trP

    type(lattice_domains_type) :: dom
    integer :: l, indx_l, m, indx_m
    integer :: nbas, norb
    type(matrix), pointer :: ptr_cl, ptr_cm, ptr_P
    type(matrix) :: X

!-tbp: stuff for testing
    type(matrix) :: PMO
    real(realk) :: EHF_trP

    nbas=cmo%nrow
    norb=cmo%ncol

    call mat_init(X,norb,nbas)
    call ltdom_init(dom, wconf%blat%dims, 2*maxval(cmo%cutoffs))
    call ltdom_getdomain(dom, 2*cmo%cutoffs, incl_redundant=.true.)
    trP=0.0e0_realk
    do m=1,dom%nelms; indx_m=dom%indcs(m)
       ptr_cm=>lts_get(cmo,-indx_m)
       if (associated(ptr_cm)) then
          call mat_zero(X)
          do l=1,dom%nelms; indx_l=dom%indcs(l)
             ptr_P=>lts_get(orthpot,indx_l,indx_m)
             if (associated(ptr_P)) then
                ptr_cl=>lts_get(cmo,-indx_l)
                if (associated(ptr_cl)) then
                   call mat_mul(ptr_cl,ptr_P,'T','N',1.0e0_realk,1.0e0_realk,X)
                endif
             endif
          enddo
          trP=trP+mat_trAB(X,ptr_cm)
       endif
    enddo
    trP=0.5e0_realk*wconf%orthpot_strength*trP
    call ltdom_free(dom)
    call mat_free(X)

    if (present(lupri)) then
       write(lupri,*) ''
       write(lupri,*) 'Contribution from orthpot     ', trP
       write(lupri,*) ''
    endif

!-tbp: testing
    call mat_init(PMO,norb,norb)
    call w_calc_orthpot_mo(wconf%blat,orthpot,cmo,PMO)
    EHF_trP=0.5e0_realk*wconf%orthpot_strength*mat_tr(PMO)
    call mat_free(PMO)

    write(lupri,*) 'TESTING: lambda/2 * Tr(P0)       ', EHF_trP
    write(lupri,*) 'TESTING: difference              ', EHF_trP-trP
    write(lupri,*) ''

  end function wannier_P_energy
  
end module wannier_HF
