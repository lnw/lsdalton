 THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2016.alpha
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Sonia Coriani,           University of Trieste,     Italy     (response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Trygve Helgaker,         University of Oslo,        Norway    (supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (orbital localization, SCF optimization)
      Robert Izsak,            University of Oslo,        Norway    (ADMM)
      Branislav Jansik,        Aarhus University,         Denmark   (trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (response, integrals)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (ADMM)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (supervision)
      Simen Reine,             University of Oslo,        Norway    (integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
   This is an MPI run using       3 processes.
 Who compiled             | sisyphos
 Host                     | st-d07964.nfit.au.dk
 System                   | Linux-3.6.10-4.fc18.x86_64
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | ON
 Fortran compiler         | /usr/lib64/openmpi/bin/mpif90
 Fortran compiler version | GNU Fortran (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 C compiler               | /usr/lib64/openmpi/bin/mpicc
 C compiler version       | gcc (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 C++ compiler             | /usr/lib64/openmpi/bin/mpic++
 C++ compiler version     | unknown
 BLAS                     | /lib64/atlas/libf77blas.so;/lib64/atlas/libcblas.s
                          | o;/lib64/atlas/libatlas.so
 LAPACK                   | /lib64/atlas/libatlas.so;/lib64/atlas/liblapack.so
 Static linking           | OFF
 Last Git revision        | a142d8e13f5d976910f0ee58bc7988534f82b917
 Git branch               | master
 Configuration time       | 2015-03-02 09:45:34.598872

Start simulation
     Date and time (Linux)  : Mon Mar  2 09:46:24 2015
     Host name              : st-d07964.nfit.au.dk                    
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  Turbomole-TZVPPP                        
  2 NCH molecule                          
  ccsd/cc-pVQZ optimized                  
  AtomTypes=1 Nosymmetry Angstrom                             
  Charge=7.0 Atoms=2                                          
  N        0.000000    0.000000    0.115054                   
  N        8.000000    0.000000    0.115054                   
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **GENERAL                                                                                           
  .TIME                                                                                               
  .NOGCBASIS                                                                                          
  **PROFILE                                                                                           
  .ICHOR                                                                                              
  #.ICHOR PURE THERMITE                                                                               
  #.ICHOR ADD THERMITE                                                                                
  .ICHOR BASIS                                                                                        
  UnitTest_genD                                                                                       
  UnitTest_genD                                                                                       
  UnitTest_genD                                                                                       
  UnitTest_genD                                                                                       
  **WAVE FUNCTIONS                                                                                    
  .HF                                                                                                 
  *DENSOPT                                                                                            
  .START                                                                                              
  H1DIAG                                                                                              
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   N        x      0.0000000000
   2            y      0.0000000000
   3            z      0.2174205506
 
   4   N        x     15.1178090714
   5            y      0.0000000000
   6            z      0.2174205506
 
WARNING:  No bonds - no atom pairs are within normal bonding distances
WARNING:  maybe coordinates were in Bohr, but program were told they were in Angstrom ?
 
 Integrals calculated using            2  fragments
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  Turbomole-TZVPPP                                  
  CHARGES:    7.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE/home/sisyphos/NightlyTesting/tmp/DaltonGfortranDebugOPENMPI/build/basis/Turbomole-TZVPPP
 PRINT MOLECULE AND BASIS IN FULL INPUT
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :      104
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :      134
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   7.000  Turbomole-TZ    F             67          52
     2   7.000  Turbomole-TZ    F             67          52
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  N           1        0.00000000        0.00000000        0.21742055
     2  N           1       15.11780907        0.00000000        0.21742055
                      
                      
Atoms and basis sets
  Total number of atoms        :      2
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 N      7.000 Turbomole-TZVPPP          67       52 [11s6p3d2f1g|5s3p3d2f1g]                     
      2 N      7.000 Turbomole-TZVPPP          67       52 [11s6p3d2f1g|5s3p3d2f1g]                     
---------------------------------------------------------------------
total         14                              134      104
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is an OpenMP calculation using   2 threads.
    This is an MPI calculation using   3 processors

   You have requested Augmented Roothaan-Hall optimization
   => explicit averaging is turned off!

Expand trust radius if ratio is larger than:            0.75
Contract trust radius if ratio is smaller than:         0.25
On expansion, trust radius is expanded by a factor      1.20
On contraction, trust radius is contracted by a factor  0.70

 H1DIAG does not work well with only few saved microvectors for ARH...
 Resetting max. size of subspace in ARH linear equations to:           5

Density subspace min. method    : None                    
Density optimization : Augmented RH optimization          

 Maximum size of Fock/density queue in averaging:          10

Convergence threshold for gradient:   0.10E-03
We perform the calculation in the standard input basis
 
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
 The SCF Convergence Criteria is applied to the gradnorm in OAO basis

 End of configuration!

 >>>  CPU Time used in *INPUT is   0.00 seconds
 >>> wall Time used in *INPUT is   0.00 seconds
 =============================================
  
  Profile Subroutine:
  Number of threads:            2
  
  Ichor Integrals: T
 =============================================
 IchorProfile Routine
 Using Input Basis:UnitTest_genD       
 Using Input Basis:UnitTest_genD       
 Using Input Basis:UnitTest_genD       
 Using Input Basis:UnitTest_genD       
 Dims:          40          40          40          40
 Performing Ichor Profiling
 >>>  CPU Time used in IchorScreen is   9.85 seconds
 >>> wall Time used in IchorScreen is   5.40 seconds
 >>>  CPU Time used in Ichor4Center is  21.69 seconds
 >>> wall Time used in Ichor4Center is  12.92 seconds
 Norm of Ichor4Center:   1671.7296166564902     
 Done IchorUnitTest
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                 42608 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                    552 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                       0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                 464 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                 640 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):              1600 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):          0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):          38264 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMITEM):               1088 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECORBITAL):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECFRAG):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECAOBATCHINFO):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (fragmentAOS):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (PNOSpaceInfo):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lvec_data)):           0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lattice_cell)):        0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                        508.138 MB
  Max allocated memory, type(matrix)                   0.968 kB
  Max allocated memory, real(realk)                   20.485 MB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                        0.704 kB
  Max allocated memory, logical                      200.668 kB
  Max allocated memory, character                      1.600 kB
  Max allocated memory, AOBATCH                        0.000 Byte
  Max allocated memory, DECORBITAL                     0.000 Byte
  Max allocated memory, DECFRAG                        0.000 Byte
  Max allocated memory, BATCHTOORB                     0.000 Byte
  Max allocated memory, DECAOBATCHINFO                 0.000 Byte
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, fragmentAOS                    0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                         66.400 MB
  Max allocated memory, PNOSpaceInfo                   0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        0.000 Byte
  Max allocated memory, LSAOTENSOR                     0.000 Byte
  Max allocated memory, SLSAOTENSOR                    0.000 Byte
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                 191.320 kB
  Max allocated memory, ATOMITEM                       1.088 kB
  Max allocated memory, LSMATRIX                       0.000 Byte
  Max allocated memory, OverlapT                       0.000 Byte
  Max allocated memory, linkshell                      0.000 Byte
  Max allocated memory, integrand                      0.000 Byte
  Max allocated memory, integralitem                   0.000 Byte
  Max allocated memory, IntWork                        0.000 Byte
  Max allocated memory, Overlap                        0.000 Byte
  Max allocated memory, ODitem                         0.000 Byte
  Max allocated memory, LStensor                       0.000 Byte
  Max allocated memory, FMM                            0.000 Byte
  Max allocated memory, XC                             0.000 Byte
  Max allocated memory, Lvec_data                      0.000 Byte
  Max allocated memory, Lattice_cell                   0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

      ===============================================================
      Overall Timings for the Matrix Operations and Integral routines
               in Level 3 The Full molecular Calculation             
      Matrix Operation   # calls          CPU Time         Wall Time
      ===============================================================
      ==============================================================
   L3 TOTAL MAT                             0.0000            0.0000
      ==============================================================
   L3      II_get_nucpot       1            0.0000            0.0000
      ==============================================================
   L3 TOTAL INTEGRAL                        0.0590            1.0810
      ==============================================================
 >>>  CPU Time used in LSDALTON is  31.82 seconds
 >>> wall Time used in LSDALTON is  18.59 seconds

End simulation
     Date and time (Linux)  : Mon Mar  2 09:46:42 2015
     Host name              : st-d07964.nfit.au.dk                    
