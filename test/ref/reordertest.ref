  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
      This is output from LSDALTON (Release Dalton2011)
            Authors:                                  
      Sonia Coriani,            University of Trieste,        Italy  
      Trygve Helgaker,          University of Oslo,           Norway 
      Stinne Hoest,             Aarhus University,            Denmark
      Branislav Jansik,         Aarhus University,            Denmark
      Poul Joergensen,          Aarhus University,            Denmark
      Joanna Kauczor,           Aarhus University,            Denmark
      Thomas Kjaergaard,        Aarhus University,            Denmark
      Kasper Kristensen,        Aarhus University,            Denmark
      Jeppe Olsen,              Aarhus University,            Denmark
      Simen Reine,              University of Oslo,           Norway 
      Pawel Salek,              KTH Stockholm,                Sweden 
      Andreas J. Thorvaldsen,   University of Tromsoe,        Norway 
      Lea Thoegersen,           Aarhus University,            Denmark
      Vladimir Rybkin,          University of Oslo,           Norway 
      Vebjoern Bakken,          University of Oslo,           Norway 
      Mark Watson,              University of Oslo,           Norway 
      Andreas Krapp,            University of Oslo,           Norway 
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using SCF wave functions. The authors
     accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
      
     If results obtained with this code are published,
     an appropriate citation would be:
      
     "LSDalton, a molecular electronic structure program, Release 2011,
      written by <INSERT AUTHOR LIST>"

                    
  Revision info     
--------------------
                    
 Last commit rev          | 930f05c3a2d1b467be5cb473b02cd7a132278874
 Last commit author       | Merge: 47a6705 a60d540
 Last commit time         | Author: Kasper Kristensen <kasperk@chem.au.dk>

Start simulation
     Date and time (Linux)  : Tue Apr 30 11:08:41 2013
     Host name              : kasper-kristensens-macbook-pro.local    
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  6-31G                                   
                                          
                                          
  Atomtypes=2 Charge=0 Nosymmetry Angstrom                    
  Charge=6.0 Atoms=2                                          
  C          0.65790       -0.00446        0.06398            
  C         -0.65783        0.00446       -0.06385            
  Charge=1.0 Atoms=4                                          
  H          1.16103        0.06607        1.02388            
  H          1.33519       -0.08301       -0.78143            
  H         -1.33549        0.08301        0.78127            
  H         -1.16080       -0.06606       -1.02386            
                      
 -----------------------------------------
          PRINTING THE DALTON.INP FILE 
 -----------------------------------------
                      
  *DENSOPT                                                                                            
  $INFO                                                                                               
  DEBUG_MPI_MEM                                                                                       
  $END INFO                                                                                           
  **DEC                                                                                               
  .SkipHartreeFock                                                                                    
  .TESTREORDERINGS                                                                                    
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates: 18
   Written in atomic units    

   1   C        x      1.2432508235
   2            y     -0.0084281786
   3            z      0.1209046780

   4   C        x     -1.2431185427
   5            y      0.0084281786
   6            z     -0.1206590137

   7   H        x      2.1940287333
   8            y      0.1248542057
   9            z      1.9348527940

  10   H        x      2.5231434368
  11            y     -0.1568661664
  12            z     -1.4766886928

  13   H        x     -2.5237103546
  14            y      0.1568661664
  15            z      1.4763863366

  16   H        x     -2.1935940963
  17            y     -0.1248353084
  18            z     -1.9348149995
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR BASIS
   

 Default basis set library used:
        /Users/kk/dalton3/LSDALTON/../basis                                                                                                                                                                     
 OPENING FILE/Users/kk/dalton3/LSDALTON/../basis/6-31G
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  6-31G                                             
  CHARGES:    6.0000    1.0000
                      
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :       26
    Auxiliary basisfunctions           :        0
    CABS basisfunctions                :        0
    JK-fit basisfunctions              :        0
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :       60
    Primitive Auxiliary basisfunctions :        0
    Primitive CABS basisfunctions      :        0
    Primitive JK-fit basisfunctions    :        0
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   6.000  6-31G           F             22           9
     2   6.000  6-31G           F             22           9
     3   1.000  6-31G           F              4           2
     4   1.000  6-31G           F              4           2
     5   1.000  6-31G           F              4           2
     6   1.000  6-31G           F              4           2
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  C           1        1.24325082       -0.00842818        0.12090468
     2  C           1       -1.24311854        0.00842818       -0.12065901
     3  H           1        2.19402873        0.12485421        1.93485279
     4  H           1        2.52314344       -0.15686617       -1.47668869
     5  H           1       -2.52371035        0.15686617        1.47638634
     6  H           1       -2.19359410       -0.12483531       -1.93481500
                      
                      
Atoms and basis sets
  Total number of atoms        :      6
  THE  REGULAR   is on R =   0
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
      2 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
      3 H      1.000 6-31G                      4        2 [4s|2s]                                      
      4 H      1.000 6-31G                      4        2 [4s|2s]                                      
      5 H      1.000 6-31G                      4        2 [4s|2s]                                      
      6 H      1.000 6-31G                      4        2 [4s|2s]                                      
---------------------------------------------------------------------
total         16                               60       26
---------------------------------------------------------------------
                      
 Configuration:
 ==============

   You have requested Augmented Roothaan-Hall optimization
   => explicit averaging is turned off!

Expand trust radius if ratio is larger than:            0.75
Contract trust radius if ratio is smaller than:         0.25
On expansion, trust radius is expanded by a factor      1.20
On contraction, trust radius is contracted by a factor  0.70

 Maximum size of subspace in ARH linear equations:           2

Density subspace min. method    : None                    
Density optimization : Augmented RH optimization          

 Maximum size of Fock/density queue in averaging:          10

Convergence threshold for gradient:   0.10E-03
 
We perform the calculation in the Grand Canonical basis
(see PCCP 2009, 11, 5805-5813)
To use the stanard input basis use .NOGCBASIS
 
Due to the presence of the keyword (default for correlation)
.NOGCINTEGRALTRANSFORM
We transform the input basis to the Grand Canonical
basis and perform integral evaluation using this basis
 
    The Overall Screening threshold is set to              :1.0000E-08
    The Screening threshold used for Coulomb               :1.0000E-10
    The Screening threshold used for Exchange              :1.0000E-08
    The Screening threshold used for One-electron operators:1.0000E-15
 The SCF Convergence Criteria is applied to the gradnorm in OAO basis

 End of configuration!

Matrix type: mtype_dense

 Level 1 atomic calculation on 6-31G Charge   6
 ================================================
 QQQ New  h:   1453.4201223302161     
 QQQ New  K:   282.55582341996438     
 The Coulomb energy contribution    21.726141548067766     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution   -21.726141548067766     
**************************************************************************************###
 it        E(HF)            dE(HF)      DSMexit  DSM_alpha RHshift    AO gradient     ###
******************************************************************  ********************###
  1    -30.9437925733    0.00000000000    0.00      0.00000    0.00    2.18E+00  ###
*******************************************************************************************%%%
 Trust Radius   Max element     Norm     RHshift   Ratio  Dpar/Dtot  Ndens(FIFO)    SCF it %%%
*******************************************************************************************%%%
 The Coulomb energy contribution    13.801942818824134     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution   -13.801942818824134     
  2    -32.1250931891   -1.18130061579    0.00      0.00000    0.00    1.78E+00  ###
 The Coulomb energy contribution    16.017404492789598     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution   -16.017404492789598     
  3    -32.7408650905   -0.61577190141   -1.00      0.00000    0.00    2.68E-01  ###
 The Coulomb energy contribution    16.353671645789916     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution   -16.353671645789916     
  4    -32.7524018994   -0.01153680884   -1.00      0.00000    0.00    4.88E-02  ###
 The Coulomb energy contribution    16.429301793773767     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution   -16.429301793773767     
  5    -32.7527936550   -0.00039175561   -1.00      0.00000    0.00    1.60E-04  ###

 Level 1 atomic calculation on 6-31G Charge   1
 ================================================
 QQQ New  h:   4.2459146168740753     
 QQQ New  K:   2.1413661810249121     
 The Coulomb energy contribution   0.31435582677568180     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution  -0.31435582677568180     
**************************************************************************************###
 it        E(HF)            dE(HF)      DSMexit  DSM_alpha RHshift    AO gradient     ###
******************************************************************  ********************###
  1     -0.1838770840    0.00000000000    0.00      0.00000    0.00    3.96E-01  ###
*******************************************************************************************%%%
 Trust Radius   Max element     Norm     RHshift   Ratio  Dpar/Dtot  Ndens(FIFO)    SCF it %%%
*******************************************************************************************%%%
 The Coulomb energy contribution   0.25554354699120191     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution  -0.25554354699120191     
  2     -0.2091195180   -0.02524243400    0.00      0.00000    0.00    1.08E-01  ###
 The Coulomb energy contribution   0.26635366462813836     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution  -0.26635366462813836     
  3     -0.2112208627   -0.00210134479   -1.00      0.00000    0.00    7.62E-04  ###
 The Coulomb energy contribution   0.26635366462813836     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution  -0.26635366462813836     
  4     -0.2112208627    0.00000000000   -1.00      0.00000    0.00    7.62E-04  ###
 GCSCF mode: DIIS failed and is skipped
 The Coulomb energy contribution   0.26645546428868783     
 The Exchange energy contribution   -0.0000000000000000     
 The Fock energy contribution  -0.26645546428868783     
  5     -0.2112209573   -0.00000009452   -1.00      0.00000    0.00    2.12E-04  ###
Matrix type: mtype_dense

 Initital Hartree-Fock calculation is skipped!

Total no. of matmuls used:                        30
Total no. of Fock/KS matrix evaluations:           0
Max no. of matrices allocated in Level 3:         21
 Using   0.008 GB of mem for the testarray
REFERENCE NORM1:     420.7739943772
REFERENCE NORM2:     210.3869971886
 
 TESTING 3D REORDERINGS
 **********************
 
 reorder
 r 123:      420.7739943772 : SUCCESS
 r 132:      420.7739943772 : SUCCESS
 r 213:      420.7739943772 : SUCCESS
 r 231:      420.7739943772 : SUCCESS
 r 312:      420.7739943772 : SUCCESS
 r 321:      420.7739943772 : SUCCESS
 
 reorder and add
 ra123:      420.7739943772 : SUCCESS
 ra132:      420.7739943772 : SUCCESS
 ra213:      420.7739943772 : SUCCESS
 ra231:      420.7739943772 : SUCCESS
 ra312:      420.7739943772 : SUCCESS
 ra321:      420.7739943772 : SUCCESS
 
 scale and reorder
sr 123:      210.3869971886 : SUCCESS
sr 132:      210.3869971886 : SUCCESS
sr 213:      210.3869971886 : SUCCESS
sr 231:      210.3869971886 : SUCCESS
sr 312:      210.3869971886 : SUCCESS
sr 321:      210.3869971886 : SUCCESS
 
 scale, reorder and add
sra123:      210.3869971886 : SUCCESS
sra132:      210.3869971886 : SUCCESS
sra213:      210.3869971886 : SUCCESS
sra231:      210.3869971886 : SUCCESS
sra312:      210.3869971886 : SUCCESS
sra321:      210.3869971886 : SUCCESS
 
REFERENCE NORM3:     736.8808537294
REFERENCE NORM4:     368.4404268647
 
 TESTING 4D REORDERINGS
 **********************
 
 reorder
 r 1234:      736.8808537294 : SUCCESS
 r 1243:      736.8808537294 : SUCCESS
 r 1324:      736.8808537294 : SUCCESS
 r 1342:      736.8808537294 : SUCCESS
 r 1423:      736.8808537294 : SUCCESS
 r 1432:      736.8808537294 : SUCCESS
 r 2134:      736.8808537294 : SUCCESS
 r 2143:      736.8808537294 : SUCCESS
 r 2314:      736.8808537294 : SUCCESS
 r 2341:      736.8808537294 : SUCCESS
 r 2413:      736.8808537294 : SUCCESS
 r 2431:      736.8808537294 : SUCCESS
 r 3124:      736.8808537294 : SUCCESS
 r 3142:      736.8808537294 : SUCCESS
 r 3214:      736.8808537294 : SUCCESS
 r 3241:      736.8808537294 : SUCCESS
 r 3412:      736.8808537294 : SUCCESS
 r 3421:      736.8808537294 : SUCCESS
 r 4123:      736.8808537294 : SUCCESS
 r 4132:      736.8808537294 : SUCCESS
 r 4213:      736.8808537294 : SUCCESS
 r 4231:      736.8808537294 : SUCCESS
 r 4312:      736.8808537294 : SUCCESS
 r 4321:      736.8808537294 : SUCCESS
 
 reorder and add
 ra1234:      736.8808537294 : SUCCESS
 ra1243:      736.8808537294 : SUCCESS
 ra1324:      736.8808537294 : SUCCESS
 ra1342:      736.8808537294 : SUCCESS
 ra1423:      736.8808537294 : SUCCESS
 ra1432:      736.8808537294 : SUCCESS
 ra2134:      736.8808537294 : SUCCESS
 ra2143:      736.8808537294 : SUCCESS
 ra2314:      736.8808537294 : SUCCESS
 ra2341:      736.8808537294 : SUCCESS
 ra2413:      736.8808537294 : SUCCESS
 ra2431:      736.8808537294 : SUCCESS
 ra3124:      736.8808537294 : SUCCESS
 ra3142:      736.8808537294 : SUCCESS
 ra3214:      736.8808537294 : SUCCESS
 ra3241:      736.8808537294 : SUCCESS
 ra3412:      736.8808537294 : SUCCESS
 ra3421:      736.8808537294 : SUCCESS
 ra4123:      736.8808537294 : SUCCESS
 ra4132:      736.8808537294 : SUCCESS
 ra4213:      736.8808537294 : SUCCESS
 ra4231:      736.8808537294 : SUCCESS
 ra4312:      736.8808537294 : SUCCESS
 ra4321:      736.8808537294 : SUCCESS
 
 scale and reorder
sr 1234:      368.4404268647 : SUCCESS
sr 1243:      368.4404268647 : SUCCESS
sr 1324:      368.4404268647 : SUCCESS
sr 1342:      368.4404268647 : SUCCESS
sr 1423:      368.4404268647 : SUCCESS
sr 1432:      368.4404268647 : SUCCESS
sr 2134:      368.4404268647 : SUCCESS
sr 2143:      368.4404268647 : SUCCESS
sr 2314:      368.4404268647 : SUCCESS
sr 2341:      368.4404268647 : SUCCESS
sr 2413:      368.4404268647 : SUCCESS
sr 2431:      368.4404268647 : SUCCESS
sr 3124:      368.4404268647 : SUCCESS
sr 3142:      368.4404268647 : SUCCESS
sr 3214:      368.4404268647 : SUCCESS
sr 3241:      368.4404268647 : SUCCESS
sr 3412:      368.4404268647 : SUCCESS
sr 3421:      368.4404268647 : SUCCESS
sr 4123:      368.4404268647 : SUCCESS
sr 4132:      368.4404268647 : SUCCESS
sr 4213:      368.4404268647 : SUCCESS
sr 4231:      368.4404268647 : SUCCESS
sr 4312:      368.4404268647 : SUCCESS
sr 4321:      368.4404268647 : SUCCESS
 
 scale, reorder and add
sra1234:      368.4404268647 : SUCCESS
sra1243:      368.4404268647 : SUCCESS
sra1324:      368.4404268647 : SUCCESS
sra1342:      368.4404268647 : SUCCESS
sra1423:      368.4404268647 : SUCCESS
sra1432:      368.4404268647 : SUCCESS
sra2134:      368.4404268647 : SUCCESS
sra2143:      368.4404268647 : SUCCESS
sra2314:      368.4404268647 : SUCCESS
sra2341:      368.4404268647 : SUCCESS
sra2413:      368.4404268647 : SUCCESS
sra2431:      368.4404268647 : SUCCESS
sra3124:      368.4404268647 : SUCCESS
sra3142:      368.4404268647 : SUCCESS
sra3214:      368.4404268647 : SUCCESS
sra3241:      368.4404268647 : SUCCESS
sra3412:      368.4404268647 : SUCCESS
sra3421:      368.4404268647 : SUCCESS
sra4123:      368.4404268647 : SUCCESS
sra4132:      368.4404268647 : SUCCESS
sra4213:      368.4404268647 : SUCCESS
sra4231:      368.4404268647 : SUCCESS
sra4312:      368.4404268647 : SUCCESS
sra4321:      368.4404268647 : SUCCESS
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):             0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):        0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOM):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (CCORBITAL):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (CCATOM):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):             0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                 0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ETUVoverlap):             0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FTUVoverlap):             0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                         26.661 MB
  Max allocated memory, type(matrix)                  13.616 kB
  Max allocated memory, real(realk)                   26.503 MB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                       37.348 kB
  Max allocated memory, logical                        1.092 kB
  Max allocated memory, character                      2.640 kB
  Max allocated memory, AOBATCH                        7.440 kB
  Max allocated memory, CCORBITAL                      0.000 Byte
  Max allocated memory, CCATOM                         0.000 Byte
  Max allocated memory, BATCHTOORB                     0.000 Byte
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                          0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        2.640 kB
  Max allocated memory, LSAOTENSOR                     0.608 kB
  Max allocated memory, SLSAOTENSOR                    0.000 Byte
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                 153.056 kB
  Max allocated memory, ATOM                           3.024 kB
  Max allocated memory, LSMATRIX                       1.792 kB
  Max allocated memory, OverlapT                      38.016 kB
  Max allocated memory, linkshell                      0.000 Byte
  Max allocated memory, integrand                    258.048 kB
  Max allocated memory, integralitem                 460.800 kB
  Max allocated memory, IntWork                       51.496 kB
  Max allocated memory, Overlap                        1.551 MB
  Max allocated memory, EtuvOverlap                    0.000 Byte
  Max allocated memory, FtuvOverlap                    0.000 Byte
  Max allocated memory, ODitem                         1.920 kB
  Max allocated memory, LStensor                       3.114 kB
  Max allocated memory, FMM                            0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


End simulation
     Date and time (Linux)  : Tue Apr 30 11:08:49 2013
     Host name              : kasper-kristensens-macbook-pro.local    
