#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > standalone.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Water
   Wave Function:    DFT / LDA / 6-31G / Ahlrichs-Coulomb-Fit
   Test Purpose:     Test standalone geometry optimizer in Cartesian coordinates,
                     BFGS update of initially unit Hessian
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > standalone.mol <<'%EOF%'
BASIS
6-31G Aux=Ahlrichs-Coulomb-Fit
H2O
First order minimization in Cartesian coordinates
Atomtypes=2 Generators=0
Charge=8.0 Atoms=1
O     0.0000000000       -0.2249058930        0.0000000000             
Charge=1.0 Atoms=2
H     1.4523500000        0.8996230000        0.0000000000             
H    -1.4523500000        0.8996230000        0.0000000000             
%EOF%

#######################################################################
#  DALTON INPUT 1
#######################################################################
cat > standalone.dal <<'%EOF%'
**OPTIMI
.CARTES
.INITEV
1.D0
.BAKER
.HISTOR
.MAX IT
4
**WAVE FUNCTIONS
.DFT
 LDA
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.START
H1DIAG
.RESTART
*END OF INPUT
%EOF%
#######################################################################
#  DALTON INPUT 2
#######################################################################
cat > standalone.dal.2 <<'%EOF%'
**OPTIMI
.CARTES
.INITEV
1.D0
.BAKER
**WAVE FUNCTIONS
.DFT
 LDA
*DENSOPT
.START
H1DIAG
.NOINCREM
.RESTART
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > standalone.check
cat >> standalone.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Energy at this geometry is * \: * \-75\.819" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=6
ERROR[1]="DFT ENERGY NOT CORRECT -"

CRIT1=`$GREP "2 * y * \-0?\.199478659" $log | wc -l`
CRIT2=`$GREP "4 * H * x * 1\.510297758" $log | wc -l`
CRIT3=`$GREP "5 * y * 0?\.886909383" $log | wc -l`
CRIT4=`$GREP "7 * H * x * \-1\.510297758" $log | wc -l`
CRIT5=`$GREP "8 * y * 0?\.886909383" $log | wc -l`
TEST[2]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[2]=5
ERROR[2]="NEW GEOMETRY NOT CORRECT -"

# Memory test
#CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
#TEST[3]=`expr  $CRIT1`
#CTRL[3]=1
#ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
