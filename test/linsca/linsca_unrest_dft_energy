#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_unrest_dft_energy.info <<'%EOF%'
   linsca_unrest_energy
   --------------------
   Molecule:         CN
   Wave Function:    LDA
   Test Purpose:     Check Unrestricted Linsca 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_unrest_dft_energy.mol <<'%EOF%'
ATOMBASIS
CN
===
Atomtypes=2  Generators=0 Bohr
Charge=6 Atoms=1  Basis=3-21G
C      0  0   0.0
Charge=7 Atoms=1  Basis=3-21G
N    0  0     2.0
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_unrest_dft_energy.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
LDA
*DFT INPUT
.RADINT
1e-13
.ANGINT
47
.GRID TYPE
 BECKEORIG LMG
*DENSOPT
.ARH
.NVEC
8
.OVERLAP
0.98
.CONVTHR
3.D-5
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_unrest_dft_energy.check
cat >> linsca_unrest_dft_energy.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT1=`$GREP "Final DFT energy: * -91.3587820" $log | wc -l`
TEST[1]=`expr	$CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
