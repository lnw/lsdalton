  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2013.3
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Sonia Coriani,           University of Trieste,     Italy     (response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Trygve Helgaker,         University of Oslo,        Norway    (supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (orbital localization, SCF optimization)
      Branislav Jansik,        Aarhus University,         Denmark   (trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (response, integrals)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (PARI)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (supervision)
      Simen Reine,             University of Oslo,        Norway    (integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 Who compiled             | simensr
 Host                     | kjmac10-reine.local
 System                   | Darwin-10.8.0
 CMake generator          | Unix Makefiles
 Processor                | i386
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /usr/local/bin/gfortran
 Fortran compiler version | GNU Fortran (GCC) 4.6.0
 C compiler               | /usr/local/bin/gcc
 C compiler version       | gcc (GCC) 4.6.0
 C++ compiler             | /usr/local/bin/g++
 C++ compiler version     | g++ (GCC) 4.6.0
 BLAS                     | /usr/lib/libblas.dylib
 LAPACK                   | /usr/lib/libatlas.dylib;/usr/lib/liblapack.dylib
 Static linking           | OFF
 Last Git revision        | a4ddf7d7a9e331418e1e8e919c6b99012757b69a
 Configuration time       | 2014-06-30 09:47:12.083583

Start simulation
     Date and time (Linux)  : Mon Jun 30 10:11:58 2014
     Host name              : kjmac10-reine.local                     
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  6-31G* Aux=df-def2 ADMM=3-21G           
  HCN molecule                            
                                          
  Atomtypes=3 Nosymmetry                                      
  Charge=1. Atoms=1                                           
  H   0.0   0.0    -1.0                                       
  Charge=7. Atoms=1                                           
  N   0.0   0.0     1.5                                       
  Charge=6. Atoms=1                                           
  C   0.0   0.0     0.0                                       
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **INTEGRALS                                                                                         
  .DENSFIT                                                                                            
  .ADMM1                                                                                              
  **WAVE FUNCTIONS                                                                                    
  .DFT                                                                                                
  B3LYP                                                                                               
  *DENSOPT                                                                                            
  .VanLenthe                                                                                          
  *DFT INPUT                                                                                          
  .GRID5                                                                                              
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  9
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z     -1.0000000000
 
   4   N        x      0.0000000000
   5            y      0.0000000000
   6            z      1.5000000000
 
   7   C        x      0.0000000000
   8            y      0.0000000000
   9            z      0.0000000000
 

WARNING: Number of short HX and YX bond lengths:    1    1
WARNING: If not intentional, maybe your coordinates were in Angstrom,
WARNING: but "Angstrom" was not specified in .mol file
 
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  6-31G*                                            
  CHARGES:    1.0000    7.0000    6.0000
                      
 BASISSETLIBRARY  : AUXILIARY
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  df-def2                                           
  CHARGES:    1.0000    7.0000    6.0000
                      
 BASISSETLIBRARY  : ADMM     
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  3-21G                                             
  CHARGES:    1.0000    7.0000    6.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE/Users/simensr/Dalton/2014/build-omp/basis/6-31G*
   
  CALLING BUILD BASIS WITH DEFAULT AUXILIARY BASIS
   
 OPENING FILE/Users/simensr/Dalton/2014/build-omp/basis/df-def2
   
  CALLING BUILD BASIS WITH DEFAULT ADMM      BASIS
   
 OPENING FILE/Users/simensr/Dalton/2014/build-omp/basis/3-21G
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :       30
    Auxiliary basisfunctions           :      170
    ADMM basisfunctions                :       20
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :       58
    Primitive Auxiliary basisfunctions :      214
    Primitive ADMM basisfunctions      :       33
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis      Auxiliarybasisset   Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   1.000  6-31G*        df-def2                 F              4           2
     2   7.000  6-31G*        df-def2                 F             27          14
     3   6.000  6-31G*        df-def2                 F             27          14
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  H           1        0.00000000        0.00000000       -1.00000000
     2  N           1        0.00000000        0.00000000        1.50000000
     3  C           1        0.00000000        0.00000000        0.00000000
                      
                      
Atoms and basis sets
  Total number of atoms        :      3
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 H      1.000 6-31G*                     4        2 [4s|2s]                                      
      2 N      7.000 6-31G*                    27       14 [10s4p1d|3s2p1d]                             
      3 C      6.000 6-31G*                    27       14 [10s4p1d|3s2p1d]                             
---------------------------------------------------------------------
total         14                               58       30
---------------------------------------------------------------------
                      
                      
Atoms and basis sets
  Total number of atoms        :      3
  THE  AUXILIARY is on R =   2
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 H      1.000 df-def2                   20       18 [4s2p2d|2s2p2d]                              
      2 N      7.000 df-def2                   97       77 [14s10p6d2f1g|10s8p4d2f1g]                   
      3 C      6.000 df-def2                   97       75 [14s10p6d2f1g|10s8p5d1f1g]                   
---------------------------------------------------------------------
total         14                              214      170
---------------------------------------------------------------------
                      
                      
Atoms and basis sets
  Total number of atoms        :      3
  THE  ADMM      is on R =   7
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 H      1.000 3-21G                      3        2 [3s|2s]                                      
      2 N      7.000 3-21G                     15        9 [6s3p|3s2p]                                  
      3 C      6.000 3-21G                     15        9 [6s3p|3s2p]                                  
---------------------------------------------------------------------
total         14                               33       20
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is an OpenMP calculation using   4 threads.
    This is a serial calculation (no MPI)
 
     This is a DFT calculation of type: B3LYP
 Weighted mixed functional:
                       VWN:    0.19000
                       LYP:    0.81000
                     Becke:    0.72000
                    Slater:    0.80000
   
  The Exchange-Correlation Grid specifications:
  Radial Quadrature : Treutler-Ahlrichs M4-T2 scheme
                      (J. Chem. Phys. (1995) 102, 346).
                      Implies also that the angular integration quality becomes Z-dependant
  Space partitioning: Becke partitioning scheme with atomic size correction
                      J. Chem. Phys. (1988) vol 88 page 2547
                      Combined with a blockwise handling of grid points
                      J. Chem. Phys. (2004) vol 121, page 2915.
                      Useful for large molecules.

  We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.2154D-16
     DFT LSint integration order range     :   [  5:  47]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.2000D+00

Density subspace min. method    : Van Lenthe modified DIIS
Density optimization : Diagonalization                    
with Van Lenthe fixed level shifts      

 Maximum size of Fock/density queue in averaging:           7

Convergence threshold for gradient:   0.10E-03
 
We perform the calculation in the Grand Canonical basis
(see PCCP 2009, 11, 5805-5813)
To use the standard input basis use .NOGCBASIS
 
Since the input basis set is a segmented contracted basis we
perform the integral evaluation in the more efficient
standard input basis and then transform to the Grand 
Canonical basis, which is general contracted.
You can force the integral evaluation in Grand 
Canonical basis by using the keyword
.NOGCINTEGRALTRANSFORM
 
    The Overall Screening threshold is set to              :1.0000E-08
    The Screening threshold used for Coulomb               :1.0000E-10
    The Screening threshold used for Exchange              :1.0000E-08
    The Screening threshold used for One-electron operators:1.0000E-15
 The SCF Convergence Criteria is applied to the gradnorm in AO basis

 End of configuration!

Matrix type: mtype_dense
 
 A set of atomic calculations are performed in order to construct
 the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
 as well as JCTC 5, 1027 (2009)
 This is done in order to use the TRILEVEL starting guess and 
 perform orbital localization
 This is Level 1 of the TRILEVEL starting guess and is performed per default.
 The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
 under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
 or orbital localization.
 

 Level 1 atomic calculation on 6-31G* Charge   1
 ================================================
 The Coulomb energy contribution   0.31435582677568180     
 The Exchange energy contribution   3.14355826775681649E-002
 The Fock energy contribution  -0.28292024409811367     
    This is an OpenMP Gridgeneration calculation using  4 threads.
Total Number of grid points:       18050

  Max allocated memory, Grid                    1.247 MB

  KS electrons/energy:    1.00000000000000   -0.22355801382064 rel.err: 0.40E-14
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1     -0.4388706805    0.00000000000    0.00      0.00000    0.00    0.0000000    8.35E-02  ###
 The Coulomb energy contribution   0.29994973903199562     
 The Exchange energy contribution   2.99949739031995580E-002
 The Fock energy contribution  -0.26995476512879607     
  KS electrons/energy:    1.00000000000001   -0.21356084580467 rel.err: 0.14E-13
  2     -0.4403059204   -0.00143523997    0.00      0.00000    0.00    0.0000000    7.00E-03  ###
 The Coulomb energy contribution   0.30102731127038229     
 The Exchange energy contribution   3.01027311270382208E-002
 The Fock energy contribution  -0.27092458014334408     
  KS electrons/energy:    1.00000000000000   -0.21431327596395 rel.err: 0.22E-14
  3     -0.4403161194   -0.00001019894   -1.00      0.00000    0.00    0.0000000    3.23E-05  ###

 Level 1 atomic calculation on 6-31G* Charge   7
 ================================================
 The Coulomb energy contribution    32.444688265465764     
 The Exchange energy contribution    1.3662540947987716     
 The Fock energy contribution   -31.078434170667016     
    This is an OpenMP Gridgeneration calculation using  4 threads.
Total Number of grid points:       30120

  Max allocated memory, Grid                    1.994 MB

  KS electrons/energy:    7.00000000002481   -6.17351663672328 rel.err: 0.35E-11
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1    -52.6551835695    0.00000000000    0.00      0.00000    0.00    0.0000000    2.92E+00  ###
 The Coulomb energy contribution    21.731335978063118     
 The Exchange energy contribution    1.1057563153659844     
 The Fock energy contribution   -20.625579662697135     
  KS electrons/energy:    7.00000000002348   -4.84265083305836 rel.err: 0.34E-11
  2    -53.6963024617   -1.04111889221    0.00      0.00000    0.00    0.0000000    2.49E+00  ###
 The Coulomb energy contribution    25.565964171257324     
 The Exchange energy contribution    1.1903116863356267     
 The Fock energy contribution   -24.375652484921719     
  KS electrons/energy:    7.00000000002389   -5.29887141439196 rel.err: 0.34E-11
  3    -54.3574335979   -0.66113113617   -1.00      0.00000    0.00    0.0000000    1.83E-01  ###
 The Coulomb energy contribution    25.879586711432523     
 The Exchange energy contribution    1.1974835261521120     
 The Fock energy contribution   -24.682103185280415     
  KS electrons/energy:    7.00000000002390   -5.33728678518781 rel.err: 0.34E-11
  4    -54.3607811558   -0.00334755791   -1.00      0.00000    0.00    0.0000000    1.17E-03  ###
 The Coulomb energy contribution    25.881413950203424     
 The Exchange energy contribution    1.1975075485466908     
 The Fock energy contribution   -24.683906401656738     
  KS electrons/energy:    7.00000000002385   -5.33748600181604 rel.err: 0.34E-11
  5    -54.3607813348   -0.00000017901   -1.00      0.00000    0.00    0.0000000    5.45E-04  ###

 Level 1 atomic calculation on 6-31G* Charge   6
 ================================================
 The Coulomb energy contribution    21.726141548067741     
 The Exchange energy contribution    1.0364791519525862     
 The Fock energy contribution   -20.689662396115164     
    This is an OpenMP Gridgeneration calculation using  4 threads.
Total Number of grid points:       30564

  Max allocated memory, Grid                    2.021 MB

  KS electrons/energy:    5.99999999976348   -4.68756611666640 rel.err:-0.39E-10
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1    -36.6678378420    0.00000000000    0.00      0.00000    0.00    0.0000000    1.77E+00  ###
 The Coulomb energy contribution    15.852756224983358     
 The Exchange energy contribution   0.89063042631771749     
 The Fock energy contribution   -14.962125798665641     
  KS electrons/energy:    5.99999999976739   -3.89553404856139 rel.err:-0.39E-10
  2    -37.4900117302   -0.82217388823    0.00      0.00000    0.00    0.0000000    1.04E+00  ###
 The Coulomb energy contribution    17.703863969028557     
 The Exchange energy contribution   0.93249332395707629     
 The Fock energy contribution   -16.771370645071482     
  KS electrons/energy:    5.99999999976554   -4.13413708342500 rel.err:-0.39E-10
  3    -37.7109169421   -0.22090521189   -1.00      0.00000    0.00    0.0000000    2.47E-02  ###
 The Coulomb energy contribution    17.755125634717601     
 The Exchange energy contribution   0.93358699619536190     
 The Fock energy contribution   -16.821538638522231     
  KS electrons/energy:    5.99999999976552   -4.14087163910347 rel.err:-0.39E-10
  4    -37.7110510530   -0.00013411090   -1.00      0.00000    0.00    0.0000000    2.65E-03  ###
 The Coulomb energy contribution    17.750625342204803     
 The Exchange energy contribution   0.93347520338641155     
 The Fock energy contribution   -16.817150138818391     
  KS electrons/energy:    5.99999999976554   -4.14026877572044 rel.err:-0.39E-10
  5    -37.7110523122   -0.00000125920   -1.00      0.00000    0.00    0.0000000    1.34E-04  ###
Matrix type: mtype_dense

 First density: Atoms in molecule guess

 The Coulomb energy contribution    73.080916668753019     
 The Exchange energy contribution   -2.6259960529793447     
 The Fock energy contribution   -70.454920615773673     
    This is an OpenMP Gridgeneration calculation using  4 threads.
Total Number of grid points:       78734

  Max allocated memory, Grid                    5.366 MB

  KS electrons/energy:   14.00000016376072  -10.28074407228939 rel.err: 0.12E-07
  Iteration 0 energy:  -93.605524596341155     
 Preparing to do S^1/2 decomposition...
 smallest accepted overlap:  0.97499999999999998     
 maximum accepted ratio in dorth:  2.99999999999999989E-002
Matrix S     has nnz=       190 sparsity:     21.111 %
Matrix AO D  has nnz=       268 sparsity:     29.778 %
  Relative convergence threshold for solver:  1.00000000000000002E-002
  SCF Convergence criteria for gradient norm:  9.99999974737875164E-005
** Get Fock matrix number   1
 The Coulomb energy contribution    82.815633519063127     
 McWeeny purified ADMM D2 matrix
    This is an OpenMP Gridgeneration calculation using  4 threads.
Total Number of grid points:       78734

  Max allocated memory, Grid                    5.366 MB

  KS electrons/energy:   14.00000025614267   -2.67013936871452 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.6639036199000827     
  KS electrons/energy:   14.00000018045801  -13.83835600969894 rel.err: 0.13E-07
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1    -91.7280773348    0.00000000000    0.00      0.00000    0.00    0.0000000    3.42E+00  ###
** Make average of the last F and D matrices
 Used simple averaging
** Get new density 
 diag%cfg_lshift:           5
No. of matmuls in get_density:     3
 >>>  CPU Time used in SCF iteration is   1.44 seconds
 >>> wall Time used in SCF iteration is   0.46 seconds
** Get Fock matrix number   2
 The Coulomb energy contribution    79.728785600694394     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025692493   -2.60154437036611 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5987587428866163     
  KS electrons/energy:   14.00000018133512  -13.49002695993900 rel.err: 0.13E-07
  2    -91.8805767554   -0.15249942068    0.00      0.00000    1.00    0.0000000    6.29E-01  ###
** Make average of the last F and D matrices
 config%decomp%nocc, nbast:           7          30
 Fov maxelm  5.92887272095977275E-002
 Queue flushed!
 Used simple averaging
** Get new density 
 diag%cfg_lshift:           5
No. of matmuls in get_density:     3
 >>>  CPU Time used in SCF iteration is   1.34 seconds
 >>> wall Time used in SCF iteration is   0.40 seconds
** Get Fock matrix number   3
 The Coulomb energy contribution    79.544186837567068     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025668308   -2.59702338393803 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5943413081077580     
  KS electrons/energy:   14.00000018095507  -13.46951562544474 rel.err: 0.13E-07
  3    -91.8846598444   -0.00408308898    0.00      0.00000    1.00    0.0000000    3.85E-01  ###
** Make average of the last F and D matrices
 Used simple averaging
** Get new density 
 diag%cfg_lshift:           5
No. of matmuls in get_density:     3
 >>>  CPU Time used in SCF iteration is   1.34 seconds
 >>> wall Time used in SCF iteration is   0.39 seconds
** Get Fock matrix number   4
 The Coulomb energy contribution    79.423964251698578     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025651156   -2.59409987610003 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5914782189563823     
  KS electrons/energy:   14.00000018068561  -13.45612850465846 rel.err: 0.13E-07
  4    -91.8862498221   -0.00158997765    0.00      0.00000    1.00    0.0000000    2.43E-01  ###
** Make average of the last F and D matrices
 Used DIIS averaging
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   1.34 seconds
 >>> wall Time used in SCF iteration is   0.39 seconds
** Get Fock matrix number   5
 The Coulomb energy contribution    79.202154508984435     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025618365   -2.58879616058160 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5862233977760525     
  KS electrons/energy:   14.00000018016563  -13.43158472339519 rel.err: 0.13E-07
  5    -91.8875057781   -0.00125595602   -1.00      0.00000    0.00    0.0000000    5.32E-03  ###
** Make average of the last F and D matrices
 Used DIIS averaging
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   1.34 seconds
 >>> wall Time used in SCF iteration is   0.39 seconds
** Get Fock matrix number   6
 The Coulomb energy contribution    79.196973042791157     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025618135   -2.58869385925728 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5861276971628513     
  KS electrons/energy:   14.00000018016255  -13.43102709302033 rel.err: 0.13E-07
  6    -91.8875075717   -0.00000179364   -1.00      0.00000    0.00    0.0000000    1.74E-03  ###
** Make average of the last F and D matrices
 Used DIIS averaging
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   1.35 seconds
 >>> wall Time used in SCF iteration is   0.40 seconds
** Get Fock matrix number   7
 The Coulomb energy contribution    79.198451996436319     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025618052   -2.58872301502386 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5861549801365413     
  KS electrons/energy:   14.00000018016130  -13.43118203966897 rel.err: 0.13E-07
  7    -91.8875072328    0.00000033896   -1.00      0.00000    0.00    0.0000000    1.36E-04  ###
** Make average of the last F and D matrices
 Used DIIS averaging
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   1.37 seconds
 >>> wall Time used in SCF iteration is   0.41 seconds
** Get Fock matrix number   8
 The Coulomb energy contribution    79.198386452872242     
 McWeeny purified ADMM D2 matrix
  KS electrons/energy:   14.00000025618082   -2.58872210736620 rel.err: 0.18E-07
 ADMM exchange energy contribution:   -2.5861541984654224     
  KS electrons/energy:   14.00000018016175  -13.43117679593618 rel.err: 0.13E-07
  8    -91.8875072674   -0.00000003467   -1.00      0.00000    0.00    0.0000000    9.08E-06  ###
SCF converged in      8 iterations
 >>>  CPU Time used in **ITER is  10.87 seconds
 >>> wall Time used in **ITER is   3.22 seconds

Total no. of matmuls in SCF optimization:        307

 Number of occupied orbitals:           7
 Number of virtual orbitals:          23

 Number of occupied orbital energies to be found:           1
 Number of virtual orbital energies to be found:           1


Calculation of HOMO-LUMO gap
============================

Calculation of occupied orbital energies converged in     5 iterations!

Calculation of virtual orbital energies converged in     9 iterations!

    E(LUMO):                         0.183674 au
   -E(HOMO):                        -0.336497 au
   ------------------------------
    HOMO-LUMO Gap (iteratively):     0.520170 au


********************************************************
 it       dE(HF)          exit   RHshift    RHinfo 
********************************************************
  1    0.00000000000    0.0000    0.0000    0.0000000
  2   -0.15249942068    0.0000    1.0000    0.0000000
  3   -0.00408308898    0.0000    1.0000    0.0000000
  4   -0.00158997765    0.0000    1.0000    0.0000000
  5   -0.00125595602   -1.0000    0.0000    0.0000000
  6   -0.00000179364   -1.0000    0.0000    0.0000000
  7    0.00000033896   -1.0000    0.0000    0.0000000
  8   -0.00000003467   -1.0000    0.0000    0.0000000

======================================================================
                   LINSCF ITERATIONS:
  It.nr.               Energy                 AO Gradient norm
======================================================================
    1           -91.72807733476474822965      0.341982168847591D+01
    2           -91.88057675544536095913      0.629052859565735D+00
    3           -91.88465984442584044700      0.384722370950758D+00
    4           -91.88624982207295488479      0.243323022780351D+00
    5           -91.88750577809565811549      0.532070390160564D-02
    6           -91.88750757173701799729      0.174257224745555D-02
    7           -91.88750723277927079380      0.135681989172660D-03
    8           -91.88750726744532926205      0.908305281541681D-05

      SCF converged !!!! 
         >>> Final results from LSDALTON <<<


      Final DFT energy:                  -91.887507267445
      Nuclear repulsion:                  36.800000000000
      Electronic energy:                -128.687507267445

 >>>  CPU Time used in *SCF is  11.16 seconds
 >>> wall Time used in *SCF is   3.50 seconds

Total no. of matmuls used:                       315
Total no. of Fock/KS matrix evaluations:           9
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                      0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                       0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):          0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMITEM):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECORBITAL):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECFRAG):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECAOBATCHINFO):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (PNOSpaceInfo):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lvec_data)):           0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lattice_cell)):        0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                         72.000 MB
  Max allocated memory, type(matrix)                 701.680 kB
  Max allocated memory, real(realk)                   28.785 MB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                      444.774 kB
  Max allocated memory, logical                      201.092 kB
  Max allocated memory, character                      2.880 kB
  Max allocated memory, AOBATCH                       84.672 kB
  Max allocated memory, DECORBITAL                     0.000 Byte
  Max allocated memory, DECFRAG                        0.000 Byte
  Max allocated memory, BATCHTOORB                     0.000 Byte
  Max allocated memory, DECAOBATCHINFO                 0.000 Byte
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                         68.000 MB
  Max allocated memory, PNOSpaceInfo                   0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        9.768 kB
  Max allocated memory, LSAOTENSOR                     5.472 kB
  Max allocated memory, SLSAOTENSOR                    7.728 kB
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                 459.168 kB
  Max allocated memory, ATOMITEM                       2.048 kB
  Max allocated memory, LSMATRIX                      14.784 kB
  Max allocated memory, OverlapT                     999.680 kB
  Max allocated memory, linkshell                      1.912 kB
  Max allocated memory, integrand                      1.032 MB
  Max allocated memory, integralitem                   2.903 MB
  Max allocated memory, IntWork                      273.664 kB
  Max allocated memory, Overlap                       25.378 MB
  Max allocated memory, ODitem                         7.104 kB
  Max allocated memory, LStensor                     249.412 kB
  Max allocated memory, FMM                            0.000 Byte
  Max allocated memory, XC                             0.000 Byte
  Max allocated memory, Lvec_data                      0.000 Byte
  Max allocated memory, Lattice_cell                   0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

 >>>  CPU Time used in LSDALTON is  14.70 seconds
 >>> wall Time used in LSDALTON is   4.90 seconds

End simulation
     Date and time (Linux)  : Mon Jun 30 10:12:03 2014
     Host name              : kjmac10-reine.local                     
