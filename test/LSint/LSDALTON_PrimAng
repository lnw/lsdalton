#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_PrimAng.info <<'%EOF%'
   LSDALTON_PrimAng
   -------------
   Molecule:         NH3
   Wave Function:    HF/6-31G
   Test Purpose:     check .PrimAng keyword
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_PrimAng.mol <<'%EOF%'
BASIS
STO-3G
Vibrational averaging of shieldings, magnetizabilities and polarizabilities
---------------------------------------------------
Atomtypes=2 Nosymmetry
Charge=7.0 Atoms=1
N          0.6023400            0.00000000              0.00000000
Charge=1.0 Atoms=3
H         -0.1947831            0.899791                1.558484
H         -0.1947831            0.899791               -1.558484
H         -0.1947831           -1.7995826090           -0.000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_PrimAng.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.MICROVECS
5
.NVEC
8
.CONVDYN
TIGHT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_PrimAng.check
cat >> LSDALTON_PrimAng.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final HF energy\: * \-55\.455119" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="test not correct -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
