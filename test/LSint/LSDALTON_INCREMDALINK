#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_INCREMDALINK.info <<'%EOF%'
   LSDALTON_INCREMDALINK
   -------------
   Molecule:         2 H2O molecules
   Wave Function:    B3LYP/3-21G
   Test Purpose:     Check incremental F build in LSDALTON
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_INCREMDALINK.mol <<'%EOF%'
BASIS
3-21G
Two nearby H2O
-----------------------------
Atomtypes=2 Angstrom Nosymmetry
Charge=1. Atoms=4
H                  9.241330    0.571501   -2.493601
H                 10.146266    0.571501   -3.774055
H                  7.134485    0.659897   -2.982496
H                  8.039421    0.659897   -4.262951
Charge=8. Atoms=2
O                  9.241330    0.571501   -3.453601
O                  7.134485    0.659897   -3.942496
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_INCREMDALINK.dal <<'%EOF%'
**INTEGRALS
.DALINK
.RUNMM
*FMM
.PRINT
 5
.SCREEN
 1e-10
.LMAX
 10
.TLMAX
 24
**WAVE FUNCTION
.DFT
 B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.INCREM
.CONVDYN
SLOPPY
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_INCREMDALINK.check
cat >> LSDALTON_INCREMDALINK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-151\.8578321" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
