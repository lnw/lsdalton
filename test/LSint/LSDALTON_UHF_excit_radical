#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
	cat > LSDALTON_UHF_excit_radical.info <<'%EOF%'
   LSDALTON_UHF_excit_radical
   -------------
   Molecule:         Methane with -OH, -F, and Cl
   Wave Function:    HF/3-21G
   Test Purpose:     Check UHF_excit integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_UHF_excit_radical.mol <<'%EOF%'
BASIS
3-21G
Methane with -OH, -F, and Cl
b3lyp/6-31+g(d,p) optimized geo.
Atomtypes=4 Nosymmetry Angstrom
Atoms=2 Charge=1.0
H     0.520821    0.080551    1.451675
H     1.965224   -1.097271    0.053015
Atoms=1 Charge=6.0
C     0.430423    0.005605    0.364687
Atoms=1 Charge=8.0
O     1.023374   -1.101579   -0.177807
Atoms=1 Charge=9.0
F     0.989971    1.157250   -0.160167
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_UHF_excit_radical.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH FULL
.UNREST
.CONVDYN
 TIGHT
**RESPONS
*LINRSP
.NEXCIT
 2
*SOLVER
.AOPREC
.AOSTART
.CONVDYN
 TIGHT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_UHF_excit_radical.check
cat >> LSDALTON_UHF_excit_radical.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-212\.0926263" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Excitation energies
CRIT1=`$GREP " * 1 * 0?\.2866" $log | wc -l`
CRIT2=`$GREP " * 2 * 0?\.3315" $log | wc -l`
TEST[2]=`expr   $CRIT1 \+ $CRIT2` 
CTRL[2]=2
ERROR[2]="HF EXCITATION ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
