#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_magderiv.info <<'%EOF%'
   LSDALTON_magderiv
   -------------
   Molecule:         water/6-31G
   Wave Function:    HF
   Test Purpose:     Check magnetic derivative overlap integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_magderiv.mol <<'%EOF%'
ATOMBASIS
This file have been generated using the WRITE_MOLECULE_OUTPUT subroutine
Output is in Bohr
Atomtypes=2    Charge=0   Nosymmetry
Charge=1.00   Atoms=8     Basis=6-31G
H          8.67500000      0.00000000    -16.64000000
H          9.57993600      0.00000000    -17.92045500
H          7.20321300      0.03775700    -17.67333600
H          8.10814900      0.03775700    -18.95379100
H          9.24133000      0.57150100     -2.49360100
H         10.14626600      0.57150100     -3.77405500
H          7.13448500      0.65989700     -2.98249600
H          8.03942100      0.65989700     -4.26295100
Charge=8.00   Atoms=4     Basis=6-31G
O          8.67500000      0.00000000    -17.60000000
O          7.20321300      0.03775700    -18.63333600
O          9.24133000      0.57150100     -3.45360100
O          7.13448500      0.65989700     -3.94249600
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_magderiv.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRAL
.DEBUGMAGDERIVOVERLAP
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
H1DIAG
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_magderiv.check
cat >> LSDALTON_magderiv.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "di_magderivOverlap_test Successful" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="MAGDERIV OVERLAP TEST1 NOT CORRECT -"

CRIT1=`$GREP "QQQ SxSx magderiv * \-12.19794" $log | wc -l`
CRIT2=`$GREP "QQQ SxSy magderiv * \-109.7059" $log | wc -l`
CRIT3=`$GREP "QQQ SxSz magderiv * 1.4867362" $log | wc -l`
CRIT4=`$GREP "QQQ SySy magderiv * \-8977.126" $log | wc -l`
CRIT5=`$GREP "QQQ SySz magderiv * \-173.47866" $log | wc -l`
CRIT6=`$GREP "QQQ SzSz magderiv * \-35.125698" $log | wc -l`
TEST[2]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6`
CTRL[2]=6
ERROR[2]="MAGDERIV OVERLAP TEST2 NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
