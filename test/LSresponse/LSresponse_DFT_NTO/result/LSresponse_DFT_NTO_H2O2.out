     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pablo
     Host                     | pablo-AU
     System                   | Linux-4.6.5-040605-generic
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     BLAS                     | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_gf_lp64.so;/opt/intel/composer_xe_2015.2.16
                              | 4/mkl/lib/intel64/libmkl_sequential.so;/opt/intel/
                              | composer_xe_2015.2.164/mkl/lib/intel64/libmkl_core
                              | .so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/l
                              | ib/x86_64-linux-gnu/libm.so
     LAPACK                   | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_lapack95_lp64.a;/opt/intel/composer_xe_2015
                              | .2.164/mkl/lib/intel64/libmkl_gf_lp64.so
     Static linking           | OFF
     Last Git revision        | 061c0cd31623339685f71be119e3c92c5f603f51
     Git branch               | master
     Configuration time       | 2016-10-13 16:29:34.752403
  
    The Functional chosen is a GGA type functional
    The Functional chosen contains no exact exchange contribution

         Start simulation
     Date and time (Linux)  : Thu Oct 13 16:33:42 2016
     Host name              : pablo-AU                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G                                   
    Hydrogen Peroxide                       
                                            
    AtomTypes=2 Generators=0                                                                                                
    Charge=8.0 Atoms=2                                                                                                      
    O    -0.00000000  1.40784586 -0.09885600                                                                                
    O     0.00000000 -1.40784586 -0.09885600                                                                                
    Charge=1.0 Atoms=2                                                                                                      
    H     0.69081489  1.72614891  1.56891868                                                                                
    H    -0.69081489 -1.72614891  1.56891868                                                                                
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .NOGCBASIS
    **WAVE FUNCTIONS
    .DFT
    BLYP
    *DENSOPT
    .ARH DAVID
    .CONVDYN
    TIGHT
    **RESPONS
    .NEXCIT
    2
    *NTO
    .THRES
    0.1
    .PLT
    *SOLVER
    .CONVDYN
    TIGHT
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      4
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
          3 H      1.000 6-31G                      4        2 [4s|2s]                                      
          4 H      1.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total         18                               52       22
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       22
      Primitive Regular basisfunctions   :       52
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)
    The Functional chosen is a GGA type functional
    The Functional chosen contains no exact exchange contribution
     
    The Exchange-Correlation Grid specifications:
    Radial Quadrature : Treutler-Ahlrichs M4-T2 scheme
                        (J. Chem. Phys. (1995) 102, 346).
                        Implies also that the angular integration quality becomes Z-dependant
    Space partitioning: Stratmann-Scuseria-Frisch partitioning scheme
                        Chem. Phys. Lett. (1996) vol. 213 page 257,
                        Combined with a blockwise handling of grid points
                        J. Chem. Phys. (2004) vol 121, page 2915.
                        Useful for large molecules.

    We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.5012D-13
     DFT LSint integration order range     :   [  5:  35]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.0000D+00

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:    7
 
    Due to the tightend SCF convergence threshold we also tighten the integral Threshold
    with a factor:      0.100000

    Dynamic convergence threshold for gradient:   0.42E-05
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-09
    The Screening threshold used for Coulomb               :  1.0000E-11
    The Screening threshold used for Exchange              :  1.0000E-09
    The Screening threshold used for One-electron operators:  1.0000E-16
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!


    First density: Atoms in molecule guess

    Total Number of grid points:       48788

    Max allocated memory, Grid                    2.520 MB

    Iteration 0 energy:     -151.528536625629
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  4.24264069E-06
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1   -147.6600244767    0.00000000000    0.00      0.00000    0.00    2.839E+00 ###
      2   -149.9736823916   -2.31365791489    0.00      0.00000   -0.00    1.770E+00 ###
      3   -151.1628633402   -1.18918094863    0.00      0.00000   -0.00    6.571E-01 ###
      4   -151.4366882906   -0.27382495041    0.00      0.00000   -0.00    1.435E-01 ###
      5   -151.4580527968   -0.02136450622    0.00      0.00000   -0.00    1.351E-02 ###
      6   -151.4583713724   -0.00031857561    0.00      0.00000   -0.00    3.291E-03 ###
      7   -151.4583787628   -0.00000739041    0.00      0.00000   -0.00    2.205E-04 ###
      8   -151.4583787954   -0.00000003256    0.00      0.00000   -0.00    4.033E-05 ###
      9   -151.4583787965   -0.00000000106    0.00      0.00000   -0.00    4.565E-06 ###
     10   -151.4583787965   -0.00000000001    0.00      0.00000   -0.00    1.984E-07 ###
    SCF converged in     10 iterations
    >>>  CPU Time used in SCF iterations is  10.72 seconds
    >>> wall Time used in SCF iterations is  10.72 seconds

    Total no. of matmuls in SCF optimization:       1016
 Postponing calculation of HOMO-LUMO gap to response part...

    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -2.31365791489    0.0000   -0.0000    0.0000000
      3   -1.18918094863    0.0000   -0.0000    0.0000000
      4   -0.27382495041    0.0000   -0.0000    0.0000000
      5   -0.02136450622    0.0000   -0.0000    0.0000000
      6   -0.00031857561    0.0000   -0.0000    0.0000000
      7   -0.00000739041    0.0000   -0.0000    0.0000000
      8   -0.00000003256    0.0000   -0.0000    0.0000000
      9   -0.00000000106    0.0000   -0.0000    0.0000000
     10   -0.00000000001    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1          -147.66002447668097374844      0.283881502076294D+01
        2          -149.97368239156918434674      0.177005895363789D+01
        3          -151.16286334019423520658      0.657057181616200D+00
        4          -151.43668829060879943427      0.143522486907173D+00
        5          -151.45805279682761579352      0.135096230702089D-01
        6          -151.45837137243373149431      0.329148009171384D-02
        7          -151.45837876284639378355      0.220514604015726D-03
        8          -151.45837879540903259112      0.403346049351217D-04
        9          -151.45837879646532542210      0.456498060409792D-05
       10          -151.45837879647768886571      0.198387380811096D-06

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final DFT energy:                     -151.458378796478
          Nuclear repulsion:                      36.151288508275
          Electronic energy:                    -187.609667304753




 <<< EXCITATION ENERGIES AND TRANSITION MOMENT CALCULATION (LSTDHF) >>>


 Use MO orbital energy differences to find  first guess for eigenvectors/values
  ** RSP_SOLVER MICROITERATION NUMBER           1
 Dynamic response convergence threshold set to   9.9111303357584746E-005
Residual norm for vector   1 is:   0.126720E+00    and eigenvalue =   0.17722430   It =   1 CONV = F
 MO preconditioning
Residual norm for vector   2 is:   0.715028E-01    and eigenvalue =   0.21256445   It =   1 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER           2
Residual norm for vector   1 is:   0.376876E-01    and eigenvalue =   0.17473855   It =   2 CONV = F
 MO preconditioning
Residual norm for vector   2 is:   0.182262E-01    and eigenvalue =   0.21130019   It =   2 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER           3
Residual norm for vector   1 is:   0.479195E-02    and eigenvalue =   0.17428335   It =   3 CONV = F
 MO preconditioning
Residual norm for vector   2 is:   0.651148E-02    and eigenvalue =   0.21114149   It =   3 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER           4
Residual norm for vector   1 is:   0.427430E-03    and eigenvalue =   0.17427442   It =   4 CONV = F
 MO preconditioning
Residual norm for vector   2 is:   0.128037E-02    and eigenvalue =   0.21113069   It =   4 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER           5
Residual norm for vector   1 is:   0.159643E-04    and eigenvalue =   0.17427429   It =   5 CONV = T
Residual norm for vector   2 is:   0.121116E-03    and eigenvalue =   0.21112985   It =   5 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER           6
Residual norm for vector   1 is:   0.159643E-04    and eigenvalue =   0.17427429   It =   6 CONV = T
Residual norm for vector   2 is:   0.111883E-04    and eigenvalue =   0.21112985   It =   6 CONV = T

 *** THE REQUESTED              2 SOLUTION VECTORS CONVERGED
 Write            2  excitation vectors to disk


     GENERATION OF NATURAL TRANSITION ORBITALS
     =========================================

 NTO: Squared singular values of NTOs of excited state no.    1
 NTO: (Only values greater than 10.00% are printed)

    1       1.00954      99.90791 %
 Using molecule-specific PLT gridbox with default parameters
 Gridbox parameters are:
X1,Y1,Z1:     -6.690814972        -7.726149082        -6.098855972    
deltax,deltay,deltaz:     0.3000000119        0.3000000119        0.3000000119    
nX,nY,nZ:               46              53              47
Number of gridpoints:           114586
 Using molecule-specific PLT gridbox with default parameters
 Gridbox parameters are:
X1,Y1,Z1:     -6.690814972        -7.726149082        -6.098855972    
deltax,deltay,deltaz:     0.3000000119        0.3000000119        0.3000000119    
nX,nY,nZ:               46              53              47
Number of gridpoints:           114586

 NTO: Squared singular values of NTOs of excited state no.    2
 NTO: (Only values greater than 10.00% are printed)

    1       0.99250      99.17036 %
 Using molecule-specific PLT gridbox with default parameters
 Gridbox parameters are:
X1,Y1,Z1:     -6.690814972        -7.726149082        -6.098855972    
deltax,deltay,deltaz:     0.3000000119        0.3000000119        0.3000000119    
nX,nY,nZ:               46              53              47
Number of gridpoints:           114586
 Using molecule-specific PLT gridbox with default parameters
 Gridbox parameters are:
X1,Y1,Z1:     -6.690814972        -7.726149082        -6.098855972    
deltax,deltay,deltaz:     0.3000000119        0.3000000119        0.3000000119    
nX,nY,nZ:               46              53              47
Number of gridpoints:           114586


  ******************************************************************************
  *                   ONE-PHOTON ABSORPTION RESULTS (in a.u.)                  *
  ******************************************************************************



      Excitation              Transition Dipole Moments               Oscillator
       Energies            x               y               z           Strengths
 ===============================================================================
      0.17427429     -0.01921378     -0.05930228      0.00000000      0.00045148
      0.21112985      0.00000000     -0.00000000     -0.06368155      0.00057080


  End of excitation energy calculation
    >>>  CPU Time used in LSDALTON RSP is  21.67 seconds
    >>> wall Time used in LSDALTON RSP is  21.67 seconds
 *****************************************************
 **     CPU-TIME USED IN LSDALTON RESPONSE:    21.667999999999999         **
 *****************************************************
    Total no. of matmuls used:                      1419
    Total no. of Fock/KS matrix evaluations:          11
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          4.220 MB
      Max allocated memory, type(matrix)                 285.968 kB
      Max allocated memory, real(realk)                    4.182 MB
      Max allocated memory, integer                      352.896 kB
      Max allocated memory, logical                      195.792 kB
      Max allocated memory, character                      2.480 kB
      Max allocated memory, AOBATCH                       37.888 kB
      Max allocated memory, ODBATCH                        9.680 kB
      Max allocated memory, LSAOTENSOR                     9.728 kB
      Max allocated memory, SLSAOTENSOR                    9.568 kB
      Max allocated memory, ATOMTYPEITEM                  76.528 kB
      Max allocated memory, ATOMITEM                       2.048 kB
      Max allocated memory, LSMATRIX                       4.480 kB
      Max allocated memory, OverlapT                     163.584 kB
      Max allocated memory, integrand                    258.048 kB
      Max allocated memory, integralitem                 921.600 kB
      Max allocated memory, IntWork                       53.368 kB
      Max allocated memory, Overlap                        2.842 MB
      Max allocated memory, ODitem                         7.040 kB
      Max allocated memory, LStensor                      30.310 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is  33.30 seconds
    >>> wall Time used in LSDALTON is  33.30 seconds

    End simulation
     Date and time (Linux)  : Thu Oct 13 16:34:15 2016
     Host name              : pablo-AU                                
